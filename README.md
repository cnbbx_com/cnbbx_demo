# cnbbx_com/cnbbx_demo

#### 介绍
2021 php最新脚手架：cnbbx V8.0 你值得信赖的PHP框架

#### 软件架构
php脚手架性能超高，支持命名空间软件架构说明

#### 支持 composer 扩展安装
~~~
composer require cnbbx_com/cnbbx

composer install --prefer-source
~~~

#### demo

demo 下载 git (https://gitee.com/cnbbx_com/cnbbx_demo)

1.  http://mysql.cnbbx.com/
2.  http://mysql.cnbbx.com/main/test
3.  http://mysql.cnbbx.com/user/index
4.  http://mysql.cnbbx.com/user/request/123/456?test=789

#### index.php nginx入口文件
~~~
namespace cnbbx;

defined('DS') or define('DS', DIRECTORY_SEPARATOR);
define('BASE_PATH', __DIR__ . DS . ".." . DS);

require __DIR__ . '/../vendor/autoload.php';

$http = new Http;
$http->run();
~~~

#### index.php swoole入口文件
~~~
namespace cnbbx;

defined('DS') or define('DS', DIRECTORY_SEPARATOR);
define('BASE_PATH', __DIR__ . DS . ".." . DS);

require __DIR__ . '/../vendor/autoload.php';

$http = new Http;
$http->run('swoole', 10086);
~~~

#### 使用说明
~~~
1.  git clone https://gitee.com/cnbbx_com/cnbbx.git
2.  nginx rewrite ^(.*)$ /index.php?s=$1&$args last;
3.  mysql config .env
~~~

#### 控制器调用model
~~~
<?php
// 使用查询构造器查询满足条件的数据
$test = new test();
$row = $test->find(1);
var_dump($row->name);
var_dump($row['name']);

$user = $test->where('name', 'cnbbx')->find();
echo $user->name;

// 切换数据库
$test->changeDatabase('test');

// 清空表数据
$test->truncate('user');

// 隐藏字段(隐藏的字段新增修改都无效)
$test->hidden('score');

// 添加数据
$test->insert($row);

// 添加或更新数据
$test->replace($row);

// 更新语句
$test->where('id', 1)->update($row);
$test->where('id', 1)->data($row)->save();

// 自增
$test->where('id', 1)->setInc('score', 2);

// 自减
$test->where('id', 1)->setDec('score', 2);

// 更新字段值
$test->where('id', 1)->setField('name','cnbbx');

// 删除数据
$test->delete(1);  or  $test->delete('1,2,3,4,5');

// 添加多条数据
$test->saveAll([...]);

// 开启事务
$test->startTrans();

// 提交事务
$test->commit();

// 回滚事务
$test->rollback();

// 排序
$test->where('status', 1)->order('id', 'desc')->select();

// 限制条数
$test->where('status', 1)->limit(10)->select();

// 分组
$test->where('status', 1)->limit(0, 10)->group('userId')->select();

// 去重
$test->distinct('userId')->having('score > 10')->select();

// 左连接
$row = $test->leftJoin('user', 'test.id = user.id')->select();

// 右连接
$row = $test->rightJoin('user', 'test.id = user.id')->select();

// 内连接
$row = $test->innerJoin('user', 'test.id = user.id')->select();

// 统计数据记录总数
$row = $test->count('id');

// 查找最大值
$row = $test->max('id');

// 查找最小值
$row = $test->min('id');

// 查找平均值
$row = $test->avg('id');

// 查找总和
$row = $test->sum('score');

// 空查询
$row = $test->null('score')->select();

// 非空查询
$row = $test->notNull('name')->select();

// 相等查询
$row = $test->eq('id','1')->neq('id','2')->select();

// 不相等查询
$row = $test->gt('id','0')->select();

// 大于查询
$row = $test->egt('id','0')->select();

// 小于查询
$row = $test->lt('id','0')->select();

// 小于或等于查询
$row = $test->elt('id','0')->select();

// 相似查询
$row = $test->like('id','0')->select();

// 不相似查询
$row = $test->notLike('id','0')->select();

// 区间查询
$row = $test->between('id', 1 ,2)->select();

// 不在区间查询
$row = $test->notBetween('id', 1 ,2)->select();

// in查询
$row = $test->in('id', '1,2')->select();

// not in查询
$row = $test->notIn('id', '1,2')->select();

// find_in_set查询
$row = $test->findInSet('ids', '520')->select();

// 关联查找
    * @param string $tableName 关联表名,其它库名，请使用 . 连接
    * @param string $foreignKey 关联表外键
    * @param string $primaryKey 本表主键
    * @param string $field 关联表需要查询的字段
$row = $test->with($tableName, $foreignKey, $primaryKey, $field = '')->select();

// 分页查询
     * @param integer $page 第几页，从1开始
     * @param integer $size 分页大小
$row = $test->page(1,10)->select();
~~~