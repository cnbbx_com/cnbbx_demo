<?php

namespace app\middleware;

use cnbbx\Controller;

/**
 * 用户登录检测
 */
class InUserCheck {

    /**  @var Controller $that */
    private $that;

    /**
     * InUserCheck constructor.
     * @param Controller $_this
     */
    public function __construct($_this) {
        $this->that = $_this;
        $this->checkUser();
    }

    /**
     * 检测用户情况
     */
    private function checkUser() {
    
    }

}