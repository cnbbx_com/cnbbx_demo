<?php

namespace app\middleware;

use cnbbx\Controller;

/**
 * 访问环境检查
 */
class InAppCheck {

    /**  @var Controller $that */
    private $that;

    /**
     * InAppCheck constructor.
     * @param Controller $_this
     */
    public function __construct($_this) {
        $this->that = $_this;
    }

}