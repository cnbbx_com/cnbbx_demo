<?php

namespace app\controller;

use app\middleware\InAppCheck;
use cnbbx\Controller;
use Ofcold\IdentityCard\IdentityCard;
use srcker\Captcha;

class app extends Controller
{

    protected $middleware = [InAppCheck::class];

    /**
     * 密码算法
     * @return array
     */
    public function showCode()
    {
        return json('fox.' . strrev(date("Y")) . date("mdw"));
    }

    /**
     * 密码算法
     * @return array
     */
    public function showCode2()
    {
        return json(date("mdwHi"));
    }

    /**
     * 身份证查询接口
     * @param string $idCard
     * @param string $locale
     * @return array
     */
    public function idCard($idCard = '431002199307157462', $locale = 'zh-cn')
    {
        $idCard = IdentityCard::make($idCard, $locale);
        if ($idCard === false) {
            return error('Your ID number is incorrect');
        }
        return json($idCard->toArray());
    }

    /**
     * 图形验证码生成
     * @return string
     */
    public function captcha()
    {
        $config = [
            // 验证码字符集合
            'codeSet' => '2345678abcdefhijkmnpqrstuvwxyzABCDEFGHJKLMNPQRTUVWXY',
            // 使用中文验证码
            'useZh' => true,
            // 中文验证码字符串
            'zhSet' => '中国百宝箱高效底层框架动态验证码测试',
            // 使用背景图片
            'useImgBg' => false,
            // 验证码字体大小(px)
            'fontSize' => 25,
            // 是否画混淆曲线
            'useCurve' => true,
            // 是否添加杂点
            'useNoise' => true,
            // 验证码图片高度
            'imageH' => 0,
            // 验证码图片宽度
            'imageW' => 0,
            // 验证码位数
            'length' => 4,
            // 验证码字体，不设置随机获取
            'fontttf' => '',
            // 背景颜色
            'bg' => [243, 251, 254],
        ];
        $captcha = new Captcha($config);
        $captchaObj = $captcha->entry();
        return "<html><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><meta name=\"viewport\" content=\"width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1\"><title>动态验证码</title><center><img src='{$captchaObj['base64']}' /><br />{$captchaObj['code']}</center></html>";
    }

}
