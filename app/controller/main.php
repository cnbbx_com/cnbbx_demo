<?php

namespace app\controller;

use app\lib\AddressParse;
use app\model\wishing;
use cnbbx\Controller;
use cnbbx\View;
use common\status\Work;

class main extends Controller
{

    /**
     * 欢迎页面
     * @return string
     */
    public function index()
    {
        $search = input("Search");
        $wishing = new wishing();
        if (!empty($search)) {
            $wishing->like("wishing", "%{$search}%");
        }
        View::assign("data", $wishing->select());

        /*View::assign("data", [
        ['user' => '金哥', 'wishing' => '天使在我身边！', 'time' => date('Y-m-d h:i:s')],
        ['user' => 'monica', 'wishing' => '我希望有一个永远包容我的爱人！', 'time' => date('Y-m-d h:i:s')],
        ]);*/
        return View::fetch('index');
    }

    /**
     * 显示IP
     * @return string
     */
    public function showIp()
    {
        $Work = new Work();
        $Work->hour = date("H");
        return $Work->WriteCode() . ',2021新春快乐,您的IP是:' . getIp() . "\n";
    }

    /**
     * linux同步时间
     * @return string
     */
    public function syncTime()
    {
        return "d=`curl -Ss http://mysql.cnbbx.com/main/getTime`
if [ \"\$d\" = \"\" ];then
    echo '时间同步失败!';
    echo \$d;
else
    date -s \"\$d\"
    echo \"时间同步成功!\"
fi";
    }

    /**
     * 输出现在时间
     * @return false|string
     */
    public function getTime()
    {
        return date("Ymd H:i:s");
    }

    /**
     * 打印全部cookie
     * @return mixed
     */
    public function cookie()
    {
        $cookie = config('Cookie');
        return $cookie;
    }

    /**
     * 模板测试
     * @return string
     */
    public function test()
    {
        // 模板变量赋值
        View::assign('name', 'test');
        // 模板输出
        return View::fetch('test');
    }

    /**
     * phpinfo输出
     * @return string|mixed
     */
    public function PhpInfo()
    {
        return isCli() ? dump(View::exec("<?php phpinfo();", [])) : View::exec("<?php phpinfo();", []);
    }

    /**
     * 解析省市区县
     * @param string $location
     * @return string
     * @example 湖南省长沙市开福区马栏山
     */
    public function parseLocation($location = '湖南省长沙市开福区马栏山')
    {
        $str = "湖南长沙开福马栏山
广东省广州市海珠区广东省广州市海珠区广州市海珠区滨江东路珠江广场
山东省青岛市城阳区山东省青岛市城阳区红岛街道沟角社区朝洋水产有限公司
广东省东莞市厚街镇广东省东莞市厚街镇下汴丽湖大道31号鸿利汽修
陕西省渭南市临渭区陕西省渭南市临渭区朝阳大街渭南师范学院
云南省昆明市安宁市云南省昆明市安宁市昆钢蓝天城A区
贵州省黔西南布依族苗族自治州贵州省黔西南布依族苗族自治州兴义市 南环西路2号B13栋701号
贵州省贵阳市贵州省贵阳市花溪区云上村杨柳塘贵阳联洪合成材料厂（客户要求顺丰）
湖北省十堰市茅箭区湖北省十堰市茅箭区白浪经济开发区祥安小区特百惠专卖店
云南省曲靖市沾益区云南省曲靖市沾益区大为小区东门新伊坊
山东省聊城市东昌府区山东省聊城市东昌府区高新区九州安置区41号楼一单元五楼西户（货到联系）
江苏省徐州市铜山区江苏省徐州市铜山区上海路江苏师范大学泉山校区
重庆重庆市璧山区重庆重庆市璧山区重庆璧山区璧泉街道太阳堡
贵州省贵阳市清镇市贵州省贵阳市清镇市云岭东路高原明珠1栋
山东省德州市平原县山东省德州市平原县龙门街道军仓社区
江苏省盐城市盐都区江苏省盐城市盐都区吉祥星智能科技
江苏省苏州市姑苏区江苏省苏州市姑苏区天筑家园南区27-406室
安徽省滁州市琅琊区安徽省滁州市琅琊区北湖小区
河南省濮阳市华龙区河南省濮阳市华龙区东王什村
上海上海市上海上海市宝山区共江路1110号来运地产
山东省东营市广饶县山东省，东营市，广饶县大王镇，东张庄工业园区，宏盛公寓
江苏省南京市浦口区江苏省南京市浦口区泰山街道鼎泰家园
四川省成都市双流区四川省成都市双流县南湖大道远大中央公园一期六栋一单元2101
山东省淄博市周村区山东省淄博市周村区北郊镇联通路 淄博职业学院南校区
云南省昆明市西山区云南省昆明市西山区云山路和顺里一栋
陕西省安康市汉滨区陕西省安康市高新区科技路阳晨牧业对面福田汽车公司
安徽省合肥市安徽省合肥市肥西县桃花镇 汤口路中段合肥财经职业学院
贵州省黔西南布依族苗族自治州兴仁县贵州省黔西南布依族苗族自治州兴仁县真武山街道 环城南路，洗布唐丫口往红井田方向100米，春健汽车租赁中心
湖南省岳阳市岳阳楼区湖南省岳阳市岳阳楼区巴陵东路巴陵首府1701
广东省肇庆市端州区广东省肇庆市端州区港景棕榈3橦A座502房
四川省自贡市自流井区四川省自贡市高新区李家湾安置房B区14号
云南省西双版纳傣族自治州景洪市云南省西双版纳景洪市大渡岗乡茶场十一队
广东省深圳市宝安区广东省深圳市宝安区福永镇兴围南八巷十七号
上海上海市上海上海市闵行区上海市闵行区罗锦路591号
广东省深圳市福田区广东省深圳市福田区彩云路长城盛世家园二期7栋2B
北京北京市朝阳区北京北京市朝阳区建国路甲22号
湖北省襄阳市湖北省襄阳市枣阳市湖北省襄阳市枣阳市前进路宁安巷9号，
江苏省苏州市江苏省苏州市吴江区八坼镇友谊小区大套(22号)
重庆重庆市江北区重庆重庆市江北区海尔路299号
黑龙江省佳木斯市桦南县黑龙江省佳木斯市桦南县百福小区
江苏省南京市六合区江苏省南京市六合区长芦街道新犁社区如家养老院
山东省临沂市沂南县山东省临沂市沂南县辛集镇东南庄村
天津天津市河北区天津天津市河北区席厂大街东明里4-26-501
福建省泉州市晋江市福建省泉州市晋江市陈埭镇仙石村仙实路阿一面线糊附近
广东省佛山市南海区广东省 佛山市 南海区大沥镇黄岐岐海苑2座904
湖南省常德市津市市湖南省常德市津市市汪家桥街道红旗幸福湾西门卫
山东省淄博市周村区山东省淄博市周村区王村镇上沙村
浙江省温州市瑞安市浙江省温州市瑞安市云周街道杏垟村办公楼旁国态飞织厂
江西省鹰潭市贵溪市江西省鹰潭市贵溪市金屯镇上马小学（熊冬香收）
新疆维吾尔自治区哈密市伊州区丽园街道办新民四路博丰家居广场A座二楼欧铂丽家居定制
广东省佛山市顺德区广东省佛山市顺德区顺德勒流工业4路24号（新力彩印厂）
重庆重庆市重庆重庆市南岸区巨成龙湾11栋 {客户要求顺丰快递}
福建省漳州市漳浦县福建省漳州市漳浦县旧镇八一后面唐人制衣，
福建省漳州市云霄县福建省漳州市云霄县莆美镇莆东村鑫叶茶叶店旁巷子直下480号
浙江省湖州市吴兴区浙江省湖州市吴兴区南太湖南苑五幢204室
福建省漳州市龙文区福建省漳州市龙文区浦东批发市场栋18幢一单元
四川省泸州市龙马潭区四川省泸州市龙马潭区胡市镇旭东路泸州十七中
甘肃省兰州市七里河区甘肃省兰州市七里河区阿干镇下街203号
江苏省徐州市云龙区江苏省徐州市云龙区黄山街道铜山路209号徐州医科大学
浙江省金华市永康市浙江省金华市永康市神州模具城B2幢1号
江苏省南京市秦淮区江苏省南京市秦淮区五老村街道延龄巷2号清沐精选酒店杨公井店-330号
云南省怒江傈僳族自治州泸水县云南省怒江傈僳族自治州泸水县六库镇世纪鑫城B栋103室（带pos机）
湖南省长沙市开福区湖南省长沙市开福区芙蓉北路德峰小区C7栋1904
广东省深圳市宝安区广东省深圳市宝安区福永街道下十围下沙南三巷八号
广东省梅州市梅江区广东省梅州市梅江区老机场路曾宪梓中学教师宿舍
云南省昆明市盘龙区云南省昆明市盘龙区青云街道白龙寺300号西南林业大学老校区
江苏省盐城市射阳县江苏省盐城市射阳县水木清华苑 2#楼
江西省赣州市南康区江西省赣州市南康区唐江镇龙华乡
江苏省徐州市新沂市江苏省徐州市新沂市唐店镇后滩
北京北京市海淀区北京市海淀区皂君东里23号楼六单元五层1号
四川省雅安市名山区四川省雅安市名山区百丈镇
安徽省安庆市桐城市安徽省桐城市同康路山水龙城孙涛里C07栋二单元403室
安徽省宣城市宁国市安徽省宣城市宁国市鸿伟路魔法屋童装
广东省惠州市惠城区广东省惠州市惠城区江北新湖一路63号伟豪都市印记丰巢专柜
重庆重庆市开县重庆重庆市重庆开州区旭日湖畔C2
山西省临汾市汾西县山西省临汾市汾西县汾西县旧法院
云南省昆明市呈贡区云南省昆明市呈贡区雨花街道蓝光天娇城2期1栋
贵州省贵阳市南明区贵州省贵阳市南明区南厂路199号山海馆海鲜池
甘肃省甘南藏族自治州合作市甘肃省甘南藏族自治州合作市当周街道 念钦街563号甘南藏族自治州质量技术监督局家属楼三单元
云南省昆明市呈贡区云南省昆明市呈贡区雨花街道蓝光天娇城2期1栋
天津天津市滨海新区天津天津市滨海新区汉沽街道 详细地址: 中新生态城 云溪道与顺吉路交汇处天元项目部
重庆重庆市酉阳土家族苗族自治县重庆重庆市酉阳土家族苗族自治县板溪职业教育中心
广西壮族自治区柳州市柳南区广西壮族自治区柳州市柳南区广西柳州市柳南区柳州监狱
湖南省娄底市涟源市湖南省娄底市涟源市育才学校光明街206号
广东省深圳市光明新区广东省深圳市光明新区田寮村田盛路45号大城小厨
四川省甘孜藏族自治州泸定县四川省甘孜藏族自治州泸定县冷碛镇新公路
内蒙古自治区呼和浩特市新城区内蒙古自治区呼和浩特市新城区成吉思汗大街巨华亲亲尚城菜鸟驿站
上海上海市青浦区上海市青浦区赵巷镇新光村72号（小房子）
广东省梅州市兴宁市广东省梅州市兴宁市宁新街道办事处高陂中心屋2号。
河北省邢台市威县河北省邢台市威县章台镇
浙江省温州市平阳县浙江省温州市平阳县万全镇榆垟社区京信路1号，温州格罗孛活塞环有限公司
河南省郑州市金水区河南省郑州市金水区周庄村，秦小敏
江西省赣州市南康区江西省赣州市南康区江西省赣州市南康区上垇路怡佳商行代收
广西壮族自治区柳州市融安县广西壮族自治区柳州市融安县红卫工业区绿源木业门卫室
河南省郑州市郑东新区河南省郑州市郑东新区龙源嘉苑二期18号楼
江苏省盐城市射阳县江苏省盐城市射阳县水木清华苑 2#楼
云南省昆明市西山区云南省昆明市西山区河宏路福康花园南区 和力（收）
陕西省商洛市山阳县陕西省商洛市山阳县城关镇健达医院对面馨馨宝贝
四川省凉山彝族自治州会理县四川省凉山彝族自治州会理县新发镇
江苏省镇江市丹徒区江苏省镇江市丹徒区天盛广场3幢
陕西省渭南市韩城市陕西省渭南市韩城市西庄镇 井溢村三组
国际国际北京北京市한국 경기도 수원시 권선구 정조로471 번지201호 (세류동580-28)(상가주택) （邮编16655）TEL：01068037288
湖南省株洲市石峰区湖南省株洲市石峰区田心新概念洗车行二楼
海南省海口市秀英区海南省海口市秀英区海榆中线29号万达广场2号门鑫丰玥
江西省赣州市兴国县江西省赣州市兴国县樟木乡新街铃木摩托经销店
陕西省渭南市韩城市陕西省渭南市韩城市新城区电力局小区(水上明珠正对面)3单元12楼西户
天津天津市河东区天津天津市河东区卫国道红城7号楼1601
新疆维吾尔自治区乌鲁木齐市沙依巴克区发新疆维吾尔自治区乌鲁木齐市沙依巴克区克拉玛依东路77号乌鲁木齐友好医院一楼李英
陕西省西安市陕西省西安市灞桥区电厂东路卞家村110号（客户要求货到刷卡）
辽宁省大连市沙河口区辽宁省大连市沙河口区中山公园街道 民政街439号（科技广场步行街）20伊一艺术中心
甘肃省庆阳市庆城县甘肃省庆阳市庆城县财富广场
江苏省淮安市涟水县江苏省淮安市涟水县城南水上花月城
广东省梅州市梅江区广东省梅州市梅江区东升工业园源锋有限公司
四川省广元市利州区四川省广元市利州区下西坝火车南站三分会152栋三单元1楼
江苏省连云港市赣榆区江苏省连云港市赣榆区柘汪镇
广东省中山市小榄镇广东省中山市小榄镇联丰南路2号（第二中学后门）
山西省朔州市朔城区山西省朔州市朔城区南城街道马邑南路清水湾一期四号楼
海南省直辖县级东方市海南省直辖县级东方市八所镇港门村后海路门号10【货到请提前致电】
天津天津市东丽区天津天津市东丽区双东路景翠园门卫（货到必须刷卡，带POS机）
北京北京市顺义区北京北京市顺义区马坡镇衙门村
山西省运城市河津市山西省运城市河津市高家湾村中元巷407
江苏省苏州市昆山市江苏省苏州市昆山市花桥镇泗泾路88号庞家滨新村2号楼1502室
江苏省盐城市盐都区江苏省盐城市盐都区城南新区戴庄路29号锦盛豪庭13栋2304室
福建省厦门市思明区福建厦门思明区金山路与吕岭路交叉口西北侧宝龙一城·珑庭。2号楼
贵州省贵阳市观山湖区贵州省贵阳市观山湖区石标路金华园D区
湖北省宜昌市远安县湖北省宜昌市远安县鸣凤大道43号（奥博数码）
广东省广州市天河区广东省广州市天河区优托邦商场一楼九毛九
江苏省扬州市广陵区江苏省扬州市广陵区联谊花园1一1幢111号巴比馒
广东省惠州市博罗县广东省惠州市博罗县解放西路80号金鑫桥粮油行
内蒙古自治区乌海市海勃湾区内蒙古自治区乌海市海勃湾区千钢社区
江西省赣州市瑞金市江西省赣州市瑞金市象湖镇中山北路十四米大道连发副食店   （顺丰快递）
河北省保定市徐水区河北省保定市徐水区河北省保定市徐水区双丰大街喜娇小卖部收30/160/180
宁夏回族自治区银川市兴庆区宁夏回族自治区银川市兴庆区新华西街新宁园五单元
安徽省合肥市蜀山区安徽省合肥市蜀山区和谐花园3栋2401
云南省昭通市巧家县云南省昭通市巧家县白鹤滩镇 拖博街安置房
内蒙古自治区包头市昆都仑区内蒙古自治区包头市昆都仑区内蒙古包头市昆区西河楞速杰快递
河南省南阳市社旗县河南省南阳市社旗县（潘河街道）远达电子商务配送中心066
山东省淄博市临淄区山东省淄博市临淄区光明北区15号楼一单元
陕西省西安市灞桥区陕西省西安市灞桥区洪庆街道航天四院科研三区
湖南省怀化市鹤城区湖南省怀化市鹤城区富程路城巿花园酒店B栋1202
陕西省西安市雁塔区陕西省西安市雁塔区锦业路高新花园，11栋1单元102
云南省昆明市呈贡区云南省昆明市呈贡区雨花街道蓝光天娇城2期1栋
上海上海市上海上海市徐虹北路18弄8号，1401室（徐家汇景园）
辽宁省沈阳市沈河区辽宁省沈阳市沈河区东大营街31号美林逸墅18号1-1-1
江苏省无锡市江阴市江苏省无锡市江阴市璜土镇南郭庄村 李小艳
河南省许昌市魏都区河南省许昌市魏都区高桥营乡老吴营村泓方东厂
福建省厦门市湖里区福建省厦门市湖里区枋湖北三里古龙居住公园8号201室
江苏省南京市雨花台区江苏省南京市雨花台区宁双路19号云密城L栋1501-1504室
新疆维吾尔自治区伊犁哈萨克自治州新源县新疆维吾尔自治区伊犁哈萨克自治州伊宁市新源县糖厂家属院
湖南省益阳市南县湖南省益阳市南县大通湖区千山红镇
吉林省松原市乾安县吉林省松原市乾安县县医院东门夫妻小吃
浙江省金华市浙江省金华市东阳市白云街道东山56幢东门一楼   顺丰
湖北省武汉市洪山区湖北省武汉市洪山区华中科技大学喻园小区一期二栋四单元301
山东省淄博市桓台县山东省淄博市桓台县东周村村头天维公司工地
四川省德阳市旌阳区四川省德阳市旌阳区城南一号
浙江省杭州市临安市浙江省杭州市临安市横畈镇回坞山2号
浙江省杭州市余杭区浙江省杭州市余杭区仓前街道文一西路浙一医院东门梁培河收
上海上海市上海上海市奉贤区金钱公路718号
河南省新乡市原阳县河南省新乡市原阳县阳阿乡查地村
广东省江门市广东省江门市新会区会城街道明兴路3号3座(锦钊车行代收)
浙江省台州市温岭市浙江省 台州市 温岭市泽国镇牧屿里A一26号
山西省太原市小店区山西省太原市小店区亲贤街与建设路交叉口中正花园二期
河南省三门峡市湖滨区河南省三门峡市湖滨区大岭南路兴业公寓1号楼一单元2楼西户
四川省德阳市广汉市四川省德阳市广汉市和兴镇广汉市监管中心值班室
广东省广州市番禺区广东省广州市番禺区钟村镇105国道谢村路段羽毛球馆旁东街一街六巷4号
甘肃省兰州市榆中县甘肃省兰州市榆中县甘草店
湖北省武汉市湖北省武汉市欢乐大道东湖庭园绿色百果园
湖南省娄底市新化县湖南省娄底市新化县琅塘镇四中加油站斜对面
福建省厦门市翔安区福建省厦门市翔安区马巷镇厦门盈发实业有限公司翔安北路3988号
山西省临汾市山西省临汾市候马路西老电影院冷饮批发
天津天津市河北区天津天津市河北区建昌道泗阳里50号河北九幼
湖北省武汉市江汉区湖北省武汉市江汉区唐家墩顶琇国际城D区4栋一单元2401
四川省巴中市平昌县四川省巴中市平昌县江阳车站对面
浙江省金华市义乌市浙江省金华市义乌市赤岸镇后山路108号精粹袜业
福建省福州市闽清县福建省福州市闽清县池园镇宝新村
四川省成都市邛崃市四川省成都市邛崃市临邛镇金丰雅居5栋
辽宁省鞍山市铁东区辽宁省鞍山市铁东区平安街康宁花园21号楼
江苏省常州市江苏省苏州市吴江区联扬小区三区622，沈丽琴
山东省日照市五莲县山东省日照市五莲县人民路1067号五莲广播电视台
甘肃省兰州市城关区甘肃省兰州市城关区甘南路363号，德盛伊园2908室
广东省东莞市黄江镇广东省东莞市黄江镇玉堂围玉裕街一号
云南省西双版纳傣族自治州景洪市允景洪街道勐海路68号快达曼相矿山
湖北省黄冈市黄梅县湖北省黄冈市黄梅县小池镇张东湾村3组
四川省 凉山彝族自治州 盐源县 四川省凉山彝族自治州盐源县润盐东街293号
广东省 佛山市 顺德区 广东省佛山市顺德区乐从镇岭南大道2号中欧中心A栋211室
浙江省 宁波市 海曙区 浙江省宁波市海曙区古林镇宋严王村陈横楼博宏工业园新大楼五楼
江苏省连云港市灌南县江苏省连云港市灌南县锦绣名园11幢第一单元402室
浙江省杭州市萧山区浙江省杭州市萧山区瓜沥镇 航民村航民新公寓外来员工居住中心
黑龙江省哈尔滨市道里区黑龙江省哈尔滨市道里区新阳路紫金城3号楼2单元2402
福建省福州市福建省福州市仓山区福建省福州市仓山区城门镇胪雷新城1区
浙江省杭州市萧山区浙江省杭州市萧山区瓜沥镇 航民村航民新公寓外来员工居住中心
云南省曲靖市师宗县云南省曲靖市师宗县凯莱财富中心4栋
湖南省邵阳市城步苗族自治县湖南省邵阳市城步苗族自治县蒋坊乡柳林村四组
四川省广安市岳池县四川省广安市岳池县新中巷95号附近 快递打电话给客户联系客户去自取
广东省深圳市龙岗区广东省深圳市龙岗区中心城鸿基花园6栋401
四川省广安市岳池县四川省广安市岳池县新中巷95号附近
广东省东莞市寮步镇广东省东莞市寮步镇牛杨村金钗路挺丰科技园歌图光电
四川省达州市大竹县四川省达州市大竹县石河镇三友家私
江西省上饶市余干县江西省上饶市余干县陈坪村
湖北省武汉市硚口区湖北省武汉市硚口区解放大道古田四路长福小区B栋2单元1楼2号
浙江省宁波市镇海区浙江省宁波市镇海区经济开发区金元路308号弘美机械有限公司
江苏省苏州市张家港市江苏省苏州市张家港市杨舍镇章卿村康盛染织厂
广东省阳江市阳西县广东省阳江市阳西县阳西一中
浙江省丽水市龙泉市浙江省丽水市龙泉市松溪弄松信汽车空调厂
陕西省延安市宝塔区陕西省延安市宝塔区红化三期A区22号楼
山东省德州市德城区山东省德州市德城区玫瑰公馆
江苏省苏州市张家港市江苏省苏州市张家港市塘桥镇妙桥红星大队盛建木业
上海上海市杨浦区上海上海市杨浦区长海医院综合保障楼
福建省厦门市福建省厦门市湖里区穆厝社篮球场
湖南省长沙市望城区湖南长沙市望城区丁字新镇湘妮仓储立双物流配送中心/德福超市（湘妮仓储营销中心旁）
湖南省邵阳市湖南省邵阳市隆回县横板桥镇三溪村五组
四川省成都市金牛区四川省成都市金牛区韦家碾三路340号凤翔云庭8栋二单元1701
云南省文山壮族苗族自治州富宁县云南省文山壮族苗族自治州富宁县归朝镇
辽宁省葫芦岛市兴城市辽宁省葫芦岛市兴城市温泉街道星河小镇西星河小馆
四川省内江市隆昌县四川省内江市隆昌市古湖街道人民中路C 段川南幼儿师范高等专科学校
河北省承德市丰宁满族自治县河北省承德市丰宁满族自治县河北省承德市丰宁满族自治县大阁镇南三营村
安徽省蚌埠市淮上区安徽省蚌埠市淮上区沫河口镇三浦村
四川省广安市岳池县四川省广安市岳池县新中巷95号附近
北京北京市通州区北京市通州区梨园镇翠屏里18#271
甘肃省陇南市武都区甘肃省陇南市武都区建设路江岸名都甘肃银行
山东省济南市商河县山东省济南市商河县龙桑寺镇东小王村
江苏省连云港市灌南县江苏省连云港市灌南县文昌苑17号二单元501
四川省广元市剑阁县四川省广元市剑阁县鹤龄镇
四川省成都市青羊区四川省成都市青羊区清江西路65号
山东省青岛市莱西市青岛九联集团总部
山东省德州市德城区山东省德州市德城区天衢西路华泰景园小区6号楼1单元302室
福建省泉州市石狮市福建省泉州市石狮市高新区创业中心11号楼
新疆维吾尔自治区乌鲁木齐市沙依巴克区新疆维吾尔自治区乌鲁木齐市沙依巴克区友好北路街道观园路100号新疆师范大学温泉校区
江苏省盐城市东台市江苏省盐城市东台市江苏省东台市唐洋镇幸福路恒诚雅居
海南省海口市海南省海口市琼山区红旗镇邮政局
浙江省宁波市江北区浙江省宁波市江北区西草马路范家边9号504【货到致电】
福建省福州市平潭县福建省福州市平潭县平潭县世界城五期10号楼A10
湖南省长沙市望城区湖南省长沙市望城区东方红中路金地亚小镇15栋2单元808
四川省绵阳市涪城区四川绵阳市涪城区园艺山新庙6队黄果树
甘肃省庆阳市庆城县甘肃省庆阳市庆城县财富广场
广东省深圳市光明新区广东省深圳市光明新区公明街道唐家市场重庆鲜面店
广东省广州市荔湾区广东省广州市荔湾区芳村大道东2号（岭南V谷鹤翔小镇创意园）
陕西省汉中市略阳县陕西省汉中市略阳县两河口镇唐家沟移民社区
广西壮族自治区南宁市兴宁区广西壮族自治区南宁市兴宁区秀厢大道东段81号保利爱琴海8-133菜鸟
青海省西宁市城北区青海省西宁市城北区朝阳街道园六路东200米金盘新苑
湖北省十堰市丹江口市湖北省十堰市丹江口市新港神力锻造有限公司
上海上海市闵行区上海上海市闵行区银康路76弄23号1503室
浙江省绍兴市柯桥区浙江省绍兴市柯桥区浙江省绍兴市柯桥区柯岩街道梅墅水庄一栋三单元
山东省菏泽市成武县山东省菏泽市成武县文亭湖一号3号楼风岭居
四川省巴中市南江县四川省巴中市南江县长赤镇快递公司
广东省汕头市龙湖区广东省汕头市龙湖区新溪镇东升村
浙江省湖州市安吉县浙江省湖州市浙江省湖州市安吉县孝丰306省道浙江立欣有限公司
福建省莆田市城厢区福建省莆田市城厢区霞林街道阳光棕榈城
新疆维吾尔自治区伊犁哈萨克自治州新疆维吾尔自治区伊犁哈萨克自治州霍尔果斯市地址：新疆伊犁州霍尔果斯市61团军垦东街14号小麻扎所 姓名： 电话：
云南省临沧市临翔区云南省临沧市临翔区凤翔街道学府路二号滇西科技师范学院菜鸟驿站
浙江省杭州市萧山区浙江省杭州市萧山区三丰路301号 杭州统一企业有限公司
湖南省常德市澧县湖南省常德市澧县行政服务中心5号楼2楼
辽宁省丹东市凤城市辽宁省丹东市凤城市大堡蒙古乡保林十二组恒威农
新疆维吾尔自治区克拉玛依市克拉玛依区新疆克拉玛依市克拉玛依区天山路街道林园康乐小区13号楼
广西壮族自治区南宁市西乡塘区广西壮族自治区南宁市西乡塘区安吉大道安园东路18号广西工业器材城东区11栋102号
陕西省西安市周至县陕西省西安市周至县司竹镇阿岔村12组
广西壮族自治区南宁市武鸣区广西壮族自治区南宁市武鸣县永宁路26号
福建省福州市晋安区福建省福州市晋安区鼓山镇福马路福盈创业园B栋晟寓
陕西省宝鸡市金台区陕西省宝鸡市金台区宝平路锦绣新城
湖南省益阳市安化县湖南省益阳市安化县南金乡将军完小
海南省直辖县级万宁市海南省万宁市和乐镇繁华街65号
云南省昆明市安宁市云南省昆明市安宁市丽景嘉园
广东省深圳市宝安区广东省深圳市宝安区福海街道重庆路新福工业园B3栋
河北省石家庄市鹿泉区河北省石家庄市鹿泉区动物园北门
广西壮族自治区百色市田东县广西省百色市田东县林逢镇初级中学【货到先致电】
广东省东莞市南城区广东省东莞市南城区中信森林湖兰溪谷鲜厨生活馆
云南省昆明市西山区云南省昆明市西山区华都花园A区30一3一1101
海南省直辖县级白沙黎族自治县海南省直辖县级白沙黎族自治县七坊镇七坊东路127号
湖北省武汉市黄陂区湖北省武汉市黄陂区盘龙城佳海工业园F区佳海3巷30号
浙江省绍兴市浙江省绍兴市人民东路489号
江西省赣州市章贡区江西省赣州市章贡区水南镇全南路40号阳光瑞香新城B区6.7栋102室，
内蒙古自治区呼和浩特市回民区内蒙古自治区呼和浩特市回民区中山西路街道王府井后院1单元1101室
陕西省宝鸡市扶风县陕西省宝鸡市扶风县午井镇大官村毛家（发顺丰）
安徽省合肥市巢湖市安徽省合肥市巢湖市中凯景湖豪庭2号楼2单位504室
广东省佛山市顺德区广东省佛山市顺德区北滘镇碧江新德云市场云市场68号菜档
广东省广州市白云区广东省广州市白云区白云大道北 岭南新世界 映翠园G23栋1204号
广东省惠州市惠城区广东省惠州市惠城区江北新湖一路63号，伟豪都市印记丰巢专柜
江苏省无锡市江阴市江苏省无锡市江阴市江阴市文定三村十幢506室
贵州省遵义市务川仡佬族苗族自治县贵州省遵义市务川仡佬族苗族自治县中医院后门大众超市楼上
陕西省西安市阎良区陕西省西安市阎良区开发区供电局家属院
广东省佛山市南海区广东省佛山市南海区狮山镇罗村大道南香江美食城二楼
江苏省南通市崇川区江苏省南通市崇川区江海镇区江山路腾飞新村小区西100m嘉福宾馆前台
广东省汕头市澄海区广东省汕头市澄海区凤翔街道港口定安路莲角池片2巷2号澄福公寓
江苏省苏州市江苏省苏州市平江区观前街观凤数码广场3楼3088A
安徽省芜湖市安徽省芜湖市湾沚区都市新苑
陕西省咸阳市渭城区陕西省咸阳市渭城区新兴街道西北国棉一厂西区
福建省泉州市晋江市福建省泉州市晋江市金井镇围头村西环区197号 （送货上门）
广东省东莞市常平镇广东省东莞市常平镇木伦工业区声亿电子厂
河南省安阳市殷都区河南省安阳市殷都区水冶人民路南昌泰小区门岗
河南省周口市鹿邑县河南省周口市鹿邑县贾摊镇五门行政村小米庄
河南省周口市河南省周口市鹿邑县贾摊镇五门行政村小米庄
江苏省苏州市吴江区江苏省苏州市吴江区桃源镇远欣路1838号
云南省红河哈尼族彝族自治州绿春县云南省红河哈尼族彝族自治州绿春县苗圃园8号101室
江苏省苏州市吴江区江苏省苏州市吴江区桃源镇远欣路1838号
陕西省咸阳市渭城区陕西省咸阳市渭城区陕西省咸阳市渭城区新兴街道西北国棉一厂西区
江苏省无锡市滨湖区江苏省无锡市滨湖区雪浪街道溪湾雅苑一期4单元1502
黑龙江省牡丹江市林口县黑龙江省牡丹江市林口县百合小区二号楼三单元202室
北京北京市怀柔区北京北京市怀柔区庙城镇小杜两河村村牌西万里行收
新疆维吾尔自治区直辖县级石河子市新疆维吾尔自治区直辖县级石河子市石总场四分场泉水地，泉水花园16栋321
江苏省扬州市江都区江苏省扬州市江都区江苏省扬州市江都区中惠集团华山路588号
广东省东莞市长安镇广东省东莞市长安镇新民三村新丰路二巷六号
湖北省襄阳市谷城县湖北省襄阳市谷城县谢湾镇黄峪铺村一组、
广东省韶关市曲江区广东韶关市曲江区马背镇16冶山村31栋
广东省广州市越秀区广东省广州市越秀区盘福路一号广州市第一人民医院内科配送部
河北省廊坊市河北省廊坊市香河县京东环保产业园高氏印务北门
四川省成都市成华区四川省成都市成华区成都广播电视大学（成都市建设北路一段七号）
河北省唐山市河北省唐山市迁安市 河北省唐山市迁安市沙河驿镇沙河驿村
甘肃省定西市临洮县甘肃省定西市临洮县中铺镇上铺村
福建省龙岩市新罗区福建省龙岩市新罗区白沙镇虎头坪
甘肃省兰州市七里河区甘肃省兰州市七里河区秀川街道深安路土主庙对面生活区
湖北省黄冈市蕲春县湖北省黄冈市蕲春县檀林镇楼花村
福建省泉州市晋江市福建省泉州市晋江市陈埭镇溪边老三兴厂
浙江省杭州市淳安县浙江省杭州市淳安县临岐镇文浦路47号
贵州省贵阳市花溪区贵州省贵阳市花溪区溪北路园亭山小区B栋二单元一楼
福建省莆田市城厢区福建省莆田市城厢区龙桥街道泗水雅居12号楼35O1号郑银芳
广东省东莞市南城区广东东莞省东莞市南城区莞太路21号路段美佳大夏B406
宁夏回族自治区银川市兴庆区宁夏回族自治区银川市兴庆区丽景街塞上凝聚力5－15
江西省抚州市东乡县江西省抚州市东乡县红星工业园区远平米厂
福建省泉州市丰泽区福建省泉州市丰泽区东湖街道尚园小区10幢904
浙江省衢州市衢江区浙江省衢州市衢江区阳光新屋里28幢二单元202室
福建省福州市晋安区福建省福州市晋安区鼓山镇洋里佳园一号楼超市
黑龙江省哈尔滨市南岗区黑龙江省哈尔滨市南岗区高新路3号创业中心20号楼1楼工业部于
西藏自治区拉萨市城关区西藏自治区拉萨市城关区纳金乡西二路城关花园嘎吉3区
甘肃省兰州市永登县甘肃省兰州市永登县城关镇纬九路香榭国际售房部
湖北省襄阳市谷城县湖北省襄阳市谷城县城关镇大古桥村
山东省聊城市临清市山东省聊城市临清市新龙湾小区2号楼1单元1401室
上海上海市上海市杨浦区杭州路908号201室   顺丰快递
江苏省淮安市江苏省淮安市清浦区江苏淮安清浦区华能小区快递柜。
重庆重庆市重庆重庆市南岸区南坪东路海棠晓月C9-26-2
湖北省黄石市阳新县湖北省黄石市阳新县兴国镇 老党校路口
广东省深圳市宝安区广东省深圳市宝安区西乡街道碧海君庭4栋1808
广东省东莞市大朗镇广东省东莞市大朗镇大井头岭南南区四巷7号
江苏省徐州市铜山区江苏省徐州市铜山区新区街道万达路嘉乐园小区
安徽省安庆市大观区安徽省安庆市大观区皇冠路与环湖西路艾检蒙科技有限公司
四川省凉山彝族自治州盐源县盐井镇金州路248号金色阳光大酒店前台
黑龙江省哈尔滨市巴彦县黑龙江省哈尔滨市巴彦县水韵新城
陕西省宝鸡市金台区陕西省宝鸡市金台区陕西省宝鸡市金台区冠森路田丰园小区
湖北省荆门市京山县湖北省荆门市京山县雁门口镇光武大道
浙江省宁波市慈溪市浙江省宁波市慈溪市观海卫镇观海卫湖东村典杜桥菜场路3号
北京北京市朝阳区北京北京市朝阳区林翠西里48号208
广西壮族自治区北海市银海区广西壮族自治区北海市银海区 平阳镇 银滩大道 灵北市场
山东省泰安市岱岳区山东省泰安市岱岳区泰山大街403号
陕西省西安市碑林区伊顿公馆东区3号楼
云南省丽江市古城区云南省丽江市古城区香格里大道南段万通汽车城
河南省三门峡市湖滨区河南省三门峡市湖滨区大岭南路兴业公寓1号楼一单元2楼西户
河南省安阳市文峰区河南省安阳市文峰区黄河大道安阳工学院女寝
江苏省扬州市仪征市江苏省扬州市仪征市大仪镇路南北苑3井02
福建省福州市长乐市福建省福州市长乐市航城街道吴钢花园11座【客户要求 派送之前打电话通知】
江苏省无锡市滨湖区江苏省无锡市滨湖区建筑路隐秀路瑜景湾三号603
福建省泉州市石狮市福建省泉州市石狮市湖滨街道琼林北路178号
湖南省常德市桃源县湖南省常德市桃源县漆河镇十字路口万豪香萝莎
河南省驻马店市西平县河南省驻马店市西平县未来大道京都高尔夫售楼部西门
北京北京市平谷区北京北京市平谷区平谷镇和平街村北斜街六号 孟彬彬
江苏省南通市海门市江苏省南通市海门市海门市城北新村404幢（货到提前致电）
辽宁省沈阳市和平区辽宁省沈阳市和平区文安路56-1号 五里河家园A座0703
山东省德州市山东省德州市经济开发区乐普大道宏昊物业院内
广西壮族自治区河池市都安瑶族自治县广西壮族自治区河池市都安瑶族自治县地苏乡地苏中心幼儿园
云南省昆明市呈贡区云南省昆明市呈贡区滇池星城星虹湾12幢
湖北省襄阳市保康县湖北省襄阳市保康县城关镇河西广电小区
新疆维吾尔自治区昌吉回族自治州吉木萨尔县新疆昌吉州吉木萨尔县北庭明珠
广西壮族自治区崇左市扶绥县广西壮族自治区崇左市扶绥县南密开发区东三巷208号自取
广东省湛江市遂溪县广东省湛江市遂溪县遂城镇文明新苑小区
重庆重庆市九龙坡区重庆市九龙坡区创新大道11号协信天骄城9-15-3
山东省临沂市沂水县山东省临沂市沂水县金龙湾小区
广西壮族自治区防城港市港口区广西防城港市港口区企沙镇柳钢基地
西藏自治区日喀则市桑珠孜区西藏自治区日喀则市桑珠孜区城北街道邦佳路一居委安多手工瓶子店
四川省广元市利州区四川省广元市利州区南河金鹭名苑
贵州省贵阳市修文县贵州省贵阳市修文县龙场镇翠微苑公租房
贵州省六盘水市盘县贵州省六盘水市盘县洒基镇云中路
贵州省遵义市正安县贵州省遵义市正安县凤仪街道城南新天广场叮当猫店
贵州省贵阳市修文县贵州省贵阳市修文县龙场镇翠微苑公租房
云南省昆明市盘龙区云南省昆明市盘龙区龙泉路570号龙溪花园8栋1单元
辽宁省朝阳市喀喇沁左翼蒙古族自治县辽宁省朝阳市喀喇沁左翼蒙古族自治县大城子镇建安社区一品城二期14号楼三单501
宁夏回族自治区银川市宁夏回族自治区银川市兴庆区丽景街塞上凝聚力5－15
广东省惠州市博罗县广东省惠州市博罗县罗阳街道麻石巷70号
广东省深圳市龙华新区观澜章阁老村东区103栋
宁夏回族自治区银川市兴庆区宁夏回族自治区银川市兴庆区永安巷拾城塾12-904
河北省石家庄市新华区河北省石家庄市新华区友谊南大街166号，西苑小区南院12-1
湖南省益阳市赫山区湖南省益阳市赫山区沧水铺镇
浙江省宁波市北仑区浙江省   宁波市  北仑区  新矸街道  高凤新村14幢608
河北省唐山市遵化市河北省唐山市遵化市依水现代城9-1-3501
广西壮族自治区贺州市昭平县广西壮族自治区贺州市昭平县木格乡木格街河东138号
云南省普洱市墨江哈尼族自治县云南省普洱市墨江哈尼族自治县兰庭园2栋三单元201
湖北省孝感市云梦县湖北省孝感市云梦县城关镇发源时代小区17栋603室
山东省济宁市汶上县山东省济宁市汶上县中都大街北段美鲜冻品行
贵州省贵阳市花溪区贵州省贵阳市花溪区小河清水江路211号大兴星城金佰利D6栋3单元3楼1号
湖南省衡阳市衡东县湖南省衡阳市衡东县城关镇东风路同利广告旁
贵州省遵义市汇川区贵州省遵义市汇川区人民路检察院森林苑小区B座17-4【货到先致电】
广东省广州市白云区广东省广州市白云区梅宾北路鸿通花苑199号，
湖南省长沙市望城区高塘岭镇，腾飞路二段19号。（中午或者晚上派送，其他时间不方便）
江苏省苏州市昆山市江苏省苏州市昆山市陆家镇南粮路园丁新村9号楼101
湖北省襄阳市大庆东路227号（十九中对面）
甘肃省兰州市甘肃省兰州市兰州新区瑞玲雅苑
湖南省湘潭市雨湖区湖南省湘潭市雨湖区韶山中路69号湘潭电脑城新浪潮信息科技有限公司
江苏省镇江市句容市江苏省镇江市句容市后白镇芦江村
四川省广安市岳池县四川省广安市岳池县新中巷95号附近
福建省宁德市福鼎市福建省宁德市福鼎市桐城街道海旺路15
安徽省安庆市桐城市安徽省安庆市桐城市吕亭镇状元府
河南省南阳市邓州市河南省南阳市邓州市邓襄路与南三环 交叉口锦泰纸塑本厂
四川省成都市成华区四川省成都市成华区沙河北岸6幢二单元201杨海兵
云南省文山壮族苗族自治州富宁县云南省文山壮族苗族自治州富宁县归朝镇
江苏省南通市如皋市江苏省南通市如皋市中医院住院部三楼护士站
广东省阳江市江城区广东省阳江市江城区东风一路3号百利广场四楼安踏儿童店
青海省西宁市城东区青海省西宁市城东区互助东路10号院毛纺小区
广东省惠州市惠城区广东省惠州市惠城区芦洲镇青塘村委会
广东省广州市增城区广东省广州市增城区派谭镇邓路吓村陈屋东二巷8号
青海省西宁市湟中县青海省西宁市湟中县多吧镇多吧南街夏都家园
江苏省苏州市昆山市江苏省苏州市昆山市同丰路黄埔城市花园777号
广西壮族自治区贵港市平南县广西壮族自治区贵港市平南县大新镇大新街
浙江省杭州市萧山区浙江省杭州市萧山区瓜沥镇 航民村航民新公寓外来员工居住中心
江西省抚州市宜黄县江西省抚州市宜黄县桃陂镇丰厚工业园机械园区，志豪机械有限公司内
河北省石家庄市行唐县河北省石家庄市行唐县只里乡 贝村
河北省邯郸市丛台区河北省邯郸市丛台区联纺路48号邯郸现代丽人医院
浙江省温州市鹿城区浙江省温州市鹿城区双屿街道崇园区德花都路158正北方向工业
四川省眉山市仁寿县四川省眉山市仁寿县仁寿大道汇金天地五栋二单元
河南省周口市商水县河南省周口市商水县胡吉镇陈屯村六组
河南省郑州市新郑市河南省郑州市新郑市八千乡道南晓燕名剪
北京北京市顺义区北京市顺义区石门市场417号
四川省宜宾市翠屏区四川省宜宾市翠屏区西郊沿江路鸿德冷冻批发部，，
山西省朔州市山阴县东环路聚胜和烟酒批发
江苏省淮安市清浦区江苏省淮安市清浦区闸口街道 河南路28号台北不夜城
新疆维吾尔自治区伊犁哈萨克自治州霍城县绿岛新都
贵州省贵阳市观山湖区贵州省贵阳市观山湖区黎阳家园12组团
广东省湛江市广东省湛江市湛江开发区绿华路16号城市假日花园A区16栋首层3号——5A号尚艺图文
北京北京市怀柔区北京北京市怀柔区北京市怀柔区桥梓镇口头村
北京北京市怀柔区北京北京市怀柔区北京市怀柔区桥梓镇口头村
四川省成都市崇州市四川省成都市崇州市崇阳镇双业东路228号清溪苑
山西省临汾市大宁县山西省临汾市大宁县山西省临汾市大宁县交通局家属楼1O1
山东省菏泽市牡丹区山东省菏泽市牡丹区李村镇东高寨村
广东省清远市阳山县广东省清远市阳山县青莲镇商业街路口
广东省惠州市惠城区广东省惠州市惠城区潼侨镇联发大道德邦科技园侨鑫商务公寓
湖南省邵阳市新宁县湖南省邵阳市新宁县回龙镇，回龙工商所对门
广西壮族自治区桂林市七星区广西壮族自治区桂林市七星区建杆北路联发旭景
四川省成都市武侯区四川省成都市武侯区天仁北二街2号和平欣苑
四川省泸州市合江县四川省泸州市合江县福宝镇瓦房
贵州省六盘水市钟山区贵州省六盘水市钟山区特区路鼻舒堂
福建省泉州市洛江区福建省泉州市洛江区罗溪镇
浙江省杭州市西湖区浙江省杭州市西湖区西斗门路3号天堂软件园A幢大厅,
四川省德阳市旌阳区四川省德阳市旌阳区孝感镇澜沧江西路1号金色明天【货到先致电】
北京北京市通州区北京市通州区张家湾镇南火垡村村内华联超市对面
安徽省宿州市埇桥区安徽省宿州市埇桥区港口北路安置区2栋0702
广西壮族自治区南宁市青秀区广西壮族自治区南宁市青秀区东葛路东葛华都31号5楼520室
湖北省黄石市黄石港区湖北省黄石市黄石港区盛昌公馆3011
河南省漯河市舞阳县河南省漯河市舞阳县孟寨镇黑龙庙村【货到先致电】
北京北京市朝阳区北京北京市朝阳区中日友好医院呼吸四部
四川省成都市武侯区四川省成都市武侯区二环路南四段36号川航佳园
四川省乐山市夹江县四川省乐山市夹江县新场填江山五组
新疆维吾尔自治区吐鲁番市新疆维吾尔自治区吐鲁番市托克逊县阿乐惠镇圣雄水泥
湖南省长沙市长沙县湖南省长沙市长沙县长沙县，毛塘铺社区，吉美超市
四川省乐山市四川省乐山市凤凰路檀木家园6栋2单园4楼2号
天津天津市东丽区天津天津市东丽区军粮城新市镇军秀园21号楼
广东省惠州市博罗县广东省惠州市博罗县杨村镇东园花苑广诚
广东省佛山市三水区广东省佛山市三水区乐平镇范湖开发区广州工商学院（三水校区）
江苏省苏州市张家港市江苏省苏州市张家港市金港镇德积华昌化工门卫
河北省邢台市内丘县河北省邢台市内丘县平安大街武装部南侧桥西名烟名酒
广西壮族自治区桂林市临桂区广西壮族自治区桂林市临桂区榕山路蒋娟志卫生室
河北省石家庄市桥西区河北省石家庄市桥西区红旗南大街汇丰西路29号河北外国语学院
四川省广元市青川县四川省广元市青川县乔庄镇平安小区门卫
重庆重庆市江津区重庆市江津区白沙镇重庆工商学校
湖南省常德市武陵区湖南省常德市武陵区南坪街道东星熙城7栋1204室
福建省莆田市秀屿区福建省莆田市秀屿区笏石镇大坵（二十五中学）
贵州省毕节市贵州省毕节市纳雍县居仁街道华阳古街贵峰电子商务有限公司生活服务站G07
吉林省白山市吉林省白山市抚松县万良镇仁义村，昕研
陕西省西安市陕西省西安市雁塔区吉祥村商业街祥和雅居I号楼
北京北京市昌平区北京北京市昌平区北七家镇白庙村西街
广东省中山市广东省中山市古镇镇古镇海洲海兴路(中国建设银行)梁锦锐收
广东省深圳市广东省深圳市龙岗区内环北路6号盈科利工业园艾比斯精密五金塑胶有限公司
江西省上饶市江西省上饶市信州区金港湾人事部
湖南省怀化市湖南省怀化市鹤城区鹤城区红星路汽车摩托车市场对面刘塘路鹤洲印象小区1栋404
福建省漳州市长泰县福建省漳州市长泰县武安镇移动公司
山东省青岛市山东省青岛市平度市崔家集中庄团结村
广东省河源市紫金县广东省河源市紫金县临江镇塘尾江东花园E栋
福建省龙岩市新罗区福建省龙岩市新罗区曹溪镇中粉电厂12幢104
广东省中山市东升镇广东省中山市东升镇兆益路66号
陕西省安康市旬阳县陕西省安康市旬阳县城关镇党家坝社区（贵豪公司售楼部）
江苏省淮安市清浦区江苏省淮安市清浦区闸口街道 河南路28号台北不夜城
江苏省淮安市清浦区江苏省淮安市清浦区闸口街道 河南路28号台北不夜城
四川省达州市四川省达州市通川区金山南路西汇名都
河北省邢台市巨鹿县河北省邢台市巨鹿县苏家营乡团城村，秦海洋
上海上海市浦东新区上海上海市浦东新区红枫路300弄7号601
新疆维吾尔自治区和田地区策勒县新疆维吾尔自治区和田地区策勒县文旅局
广东省深圳市宝安区广东省深圳市坪山区龙田街道青松西路47号
江苏省无锡市宜兴市江苏省无锡市宜兴市人民中路227号华地百货2楼gxgkids
河南省信阳市河南省信阳市固始县成功大道固始县中医院 姓名；电话；
河南省信阳市固始县河南省信阳市固始县成功大道固始县中医院
河北省邯郸市曲周县河北省邯郸市曲周县槐桥乡刘李庄村
陕西省渭南市陕西省渭南市临渭区四马路信达现代城
重庆重庆市九龙坡区重庆市九龙坡区白市驿镇海龙村长江电炉厂世纪傲东
北京北京市怀柔区北京北京市怀柔区怀柔区红螺东路21号欧曼一工厂三号物流门
上海上海市普陀区上海上海市普陀区绥德路555号D栋603室（多礼米孵化基地）
广东省广州市番禺区广东省广州市番禺区市桥街东平路13巷一号陈茶老酒
江苏省苏州市吴中区江苏省苏州市吴中区甪直镇吴淞路68号
湖北省武汉市湖北省武汉市汉阳区琴断口桃花岛七里二村145号
浙江省嘉兴市海宁市浙江省嘉兴市海宁市许村镇塘桥村8组63号
河南省南阳市内乡县河南省南阳市内乡县河南省南阳市内乡县湍东镇仙鹤生活区
江苏省淮安市清浦区江苏省淮安市清浦区闸口街道 河南路28号台北不夜城
陕西省铜川市耀州区陕西省铜川市耀州区正阳路袁家村一组。
湖南省邵阳市双清区湖南省邵阳市双清区，金罗湾国际商贸城6栋1022好运超市
广东省深圳市宝安区广东省深圳市宝安区西乡街道 宝安大道4179号缤致InFun青年社区
陕西省汉中市城固县陕西省汉中市城固县34号信箱飞天小区3号楼303室
江苏省苏州市昆山市江苏省苏州市昆山市出口加工区仁宝宿舍（B区）七号楼851室
新疆维吾尔自治区和田地区于田县新疆维吾尔自治区和田地区于田县新时代
甘肃省嘉峪关市镜铁区甘肃省嘉峪关市镜铁区明珠山水郡6号楼
广东省广州市花都区广东省广州市花都区新华街花城北路汇佳超市
江苏省无锡市滨湖区江苏省无锡市滨湖区梁清路集景花园47-902
云南省昆明市官渡区云南省昆明市官渡区云秀路官南城小区菜鸟驿站
江苏省苏州市昆山市江苏省苏州市昆山市陆家镇沙蔼新村十号楼东门
湖北省黄冈市麻城市湖北省黄冈市麻城市福田河镇兴远小区5201
广西壮族自治区南宁市宾阳县广西省南宁市宾阳县宾州镇细蒙村
浙江省温州市鹿城区浙江省温州市鹿城区双屿街道鞋都三期迪索五楼蝶佩鞋业
湖南省常德市石门县湖南省常德市石门县大唐石门电厂
北京北京市丰台区北京北京市丰台区万泉盛景园小区1-2-101
辽宁省沈阳市沈北新区辽宁省沈阳市沈北新区蒲河新镇通顺街81号辽宁现代服务职业技术学院
河南省洛阳市嵩县河南省洛阳市嵩县纸房乡高速路口正对面远航校油泵
江西省吉安市永丰县江西省吉安市永丰县恩江镇郭家新农村建设第二排笫4幢
天津天津市河西区天津天津市河西区天津市河西区微山路环渤海大酒楼旁林城佳苑14门906
新疆维吾尔自治区巴音郭楞蒙古自治州库尔勒市新疆维吾尔自治区巴音郭楞蒙古自治州库尔勒市巴音东路35号地矿嘉苑1号楼1单元602室
吉林省松原市扶余市吉林省松原市扶余市财富广场金太阳老年公寓我叫杜凤岐
广西壮族自治区百色市广西壮族自治区百色市隆林各族自治县隆林中学
浙江省温州市浙江省温州市苍南县钱库镇新安东浃头300号（顺丰）
云南省昆明市五华区云南省昆明市五华区云南省昆明市五华区昆瑞路273号云安阳光城小区3幢1204室
新疆维吾尔自治区乌鲁木齐市水磨沟区乌鲁木齐市水磨沟区南湖南路街道新疆乌鲁木齐水磨沟区南湖南路西二巷四十七号南湖安居一期5号楼四单元501
广东省湛江市廉江市广东省湛江市廉江市东升农场顺丰快递分部
内蒙古自治区包头市东河区建设路31号包头医学院
湖北省宜昌市伍家岗区湖北省宜昌市伍家岗区（江山风华爰丁绿郡一栋）
广西壮族自治区玉林市玉州区广西壮族自治区玉林市玉州区玉城街道良江街华润小区
河北省沧州市青县河北省沧州市青县盘古镇四沃头村
浙江省宁波市慈溪市浙江省宁波市慈溪市古塘街道新潮塘村新区52号
广东省广州市番禺区广东省广州市番禺区南村镇沙溪大道星河湾半岛3号园8栋2402房
广西壮族自治区防城港市防城区广西壮族自治区防城港市防城区防城镇 防钦路399号800里上城三建康华汽车城旁
云南省昆明市呈贡区云南省昆明市呈贡区云南省昆明市呈贡区滇池星城星虹湾12幢
福建省泉州市晋江市福建省泉州市晋江市清蒙经济技术开发区三星街
贵州省贵阳市南明区贵州省贵阳市南明区解放西路226号美成名园
四川省凉山彝族自治州西昌市四川省凉山彝族自治州西昌市安宁镇
上海上海市浦东新区上海上海市浦东新区浦东国际机场海天壹路300
云南省大理白族自治州大理市云南省大理白族自治州下关市北区兴隆园小区
四川省成都市新都区四川省成都市新都区铭章路29号
山西省长治市郊区山西省长治市郊区潞州区长北铁建南路 光明巷
广东省佛山市南海区广东省佛山市南海区里水镇和顺和桂工业园B区顺景大道9B-8【货到先致电】
广东省佛山市广东省佛山市张槎镇海口为竹村竹巷六巷七号
安徽省阜阳市颍州区安徽省阜阳市颍州区锦华第一郡
广东省惠州市广东省惠州市惠城区鹿颈路6号聚飞光电有限公司
吉林省吉林市昌邑区吉林省吉林市昌邑区新地号街道华汇园1号楼1单元402
甘肃省金昌市金川区甘肃省金昌市金川区世纪金都17栋2单元
浙江省杭州市浙江省杭州市余杭区南苑街道翁梅二区1-2-402
江西省上饶市铅山县江西省上饶市铅山县永平镇稼轩乡八都村
浙江省湖州市吴兴区浙江省湖州市吴兴区龙泉街道潜庄公寓31幢102室
四川省遂宁市安居区四川省遂宁市安居区分水镇人民下路137—139号
广东省佛山市顺德区广东省佛山市顺德区容桂细滘居委会容桂大道南16号C1座俊壹包装有限公司
广西壮族自治区贵港市桂平市广西壮族自治区贵港市桂平市木乐镇振兴路
福建省宁德市古田县福建省宁德市古田县城西街道山坡头26号
浙江省温州市瑞安市浙江省温州市瑞安市南滨街道阁二村镇前路29号【货到先致电】
河北省唐山市开平区河北省唐山市开平区开平镇中屈庄村代街
甘肃省庆阳市合水县甘肃省庆阳市合水县南街万象花园三栋
陕西省西安市长安区陕西省西安市长安区丈八六路与锦业二路十字东尚蜂鸟小区1号楼1单元10711室
辽宁省大连市沙河口区辽宁省大连市沙河口区富宁园7号楼
云南省曲靖市麒麟区云南省曲靖市麒麟区三江国际汇都华庭4幢1层4-12、4层4-12-2、4-13-2
浙江省绍兴市柯桥区浙江省绍兴市柯桥区。马鞍镇 兴宾路780号。
陕西省咸阳市秦都区陕西省咸阳市秦都区渭阳西路街道新运佳苑C座6层1号
江苏省泰州市海陵区江苏省泰州市海陵区罡杨镇罡南村
重庆重庆市南岸区重庆重庆市南岸区弹子石海悦府6栋
上海上海市黄浦区上海市黄浦区上海市青浦区华新镇新谊村319号
甘肃省酒泉市敦煌市甘肃省酒泉市敦煌市沙洲镇沙洲北路卫生局家属楼221号
河南省新乡市河南省新乡市封丘县河南省新乡市封丘县城关乡南葛占村
福建省泉州市惠安县福建省泉州市惠安县螺阳镇狮山路惠安一建设工程有限公司，广海新景悦城项目部保安室（工地门口的保安室，不要送到售楼部去）
福建省泉州市晋江市福建省泉州市晋江市东石镇，平坑村，清满锻压公司
河南省商丘市河南省商丘市睢阳区冯桥乡孙路口张庄
安徽省芜湖市鸠江区安徽省芜湖市鸠江区鸠兹家苑111栋
四川省绵阳市安州区四川省绵阳市安州区秀水镇秀丰干道大吃小吃
江苏省南京市雨花台区江苏省南京市雨花台区宁双路19号云密城L栋1501-1504室
四川省自贡市贡井区四川省自贡市贡井区成佳镇劳动街1号
安徽省合肥市安徽省合肥市蜀山区安徽省合肥市蜀山经济开发区芙蓉路与莲花路交叉口华地润园
江苏省无锡市江阴市江苏省无锡市江阴市华士镇华西特种化纤厂
江西省南昌市进贤县江西省南昌市进贤县文港镇花园南路82号望乡源酒店
江苏省镇江市句容市江苏省镇江市句容市后白镇芦江村
江苏省南通市通州区江苏省南通市通州区兴东镇双云路口吉云超市
江苏省苏州市江苏省苏州市吴江区横扇镇大桥路万达三号
福建省泉州市永春县福建省泉州市永春县桃城镇万星文化广场1幢16楼。
广东省江门市开平市广东省江门市开平市水口镇新市路18号华莱士
陕西省延安市宝塔区陕西省延安市宝塔区临镇迎宾招待所
广东省茂名市茂南区广东省茂名市茂南区官山七路茘晶新城东区
重庆重庆市沙坪坝区重庆重庆市沙坪坝区大石村203号
陕西省延安市宝塔区陕西省延安市宝塔区临镇迎宾招待所
四川省成都市金牛区四川省成都市金牛区文家巷13号附11
江苏省泰州市海陵区江苏省泰州市海陵区中铁溪源26栋
河南省平顶山市叶县河南省平顶山市叶县廉村镇
广西壮族自治区南宁市西乡塘区广西壮族自治区南宁市西乡塘区西乡塘街道大学东路176号广西农业职业技术学院
广东省佛山市南海区广东省佛山市南海区西樵镇锦湖路中海山湖世家16栋1401
北京北京市朝阳区北京北京市朝阳区北京市朝阳区高碑店东区A区21-7后门
河南省郑州市中原区河南省郑州市中原区航海路西三环李家门新村6号楼
甘肃省陇南市宕昌县甘肃省陇南市宕昌县陇南市宕昌县第一中学
四川省成都市崇州市四川省成都市崇州市华兴路南二巷9号金鸡万人小区
云南省大理白族自治州弥渡县云南省大理州弥渡县建设路西段、天外天家居城
广东省中山市神湾镇广东省中山市神湾镇桂竹路1号江龙船艇
河北省沧州市河间市河北省沧州市河间市留古寺镇前羊店村
甘肃省兰州市榆中县甘肃省兰州市榆中县太白东路95号司法局
广东省东莞市广东省东莞市凤岗镇雁田村南山第二工业区新宝丽集团K栋一楼
浙江省宁波市海曙区浙江省宁波市海署区光溪村小溪路89弄电话
辽宁省沈阳市辽宁省沈阳市康平县宜家兴隆超市
新疆维吾尔自治区乌鲁木齐市新市区新疆乌鲁木齐市迎宾路林清园小区12号楼3单元202室
内蒙古自治区呼和浩特市赛罕区内蒙古自治区呼和浩特市赛罕区大学东街春雨小区20号楼
甘肃省武威市甘肃省武威市凉州区 甘肃省武威市凉州区碧水兰庭小区
内蒙古自治区呼和浩特市内蒙古自治区呼和浩特市新城区兴安北路正源写字楼四层
浙江省衢州市龙游县浙江省衢州市龙游县朝阳巷一弄七号二栋410室
四川省成都市金牛区四川省成都市金牛区金科苑八十一号
陕西省西安市未央区陕西省西安市未央区徐家湾街道太华北路汉渠南路交汇处东南角浩华北郡小区中区9号楼
广东省汕头市潮南区广东省汕头市潮南区陇田镇加欣酒店
广东省广州市海珠区广东省广州市海珠区南田路仁和直街趣园楼
云南省昆明市盘龙区云南省昆明市盘龙区清水木华水晶园48幢
江苏省盐城市江苏省盐城市东台市江苏省盐城市东台市城中花园，
云南省曲靖市云南省曲靖市西苑小区御康花园
福建省泉州市晋江市福建省泉州市晋江市池店洋茂村骑士公司
安徽省安庆市桐城市安徽省安庆市桐城市龙眠街道望溪东路中建材浚鑫科技有限公司
云南省昆明市安宁市云南省昆明市安宁市连然镇宁湖九号香缇花园。
云南省红河哈尼族彝族自治州蒙自市云南省红河哈尼族彝族自治州蒙自市天马路41号
山东省聊城市莘县山东省聊城市莘县朝城镇汉郡新城
河北省衡水市阜城县河北省衡水市阜城县崔庙镇群乐钟表小家电
福建省宁德市霞浦县福建省宁德市霞浦县沙头村空海大道三河桥头福宁大道项目部
新疆维吾尔自治区 乌鲁木齐市   新疆维吾尔自治区乌鲁木齐市，祁连山街德源逸品风景小区
福建省 漳州市 云霄县 福建省漳州市云霄县云霄县将军大道漳江核苑4-906室
广东省 中山市 小榄镇 广东省中山市小榄镇绩东一郑文东二街8号
四川省 绵阳市 三台县 四川省绵阳市三台县塔山镇
江苏省苏州市吴江区江苏省苏州市吴江区盛泽镇金盛花园9幢
广西壮族自治区柳州市柳城县广西壮族自治区柳州市柳城县白阳南路51号文华苑小区
湖北省恩施土家族苗族自治州建始县湖北省恩施土家族苗族自治州建始县业州镇船儿岛社区5号
浙江省金华市义乌市浙江省金华市义乌市稠江街道龙回四区55幢1单元301
江苏省淮安市洪泽县江苏省淮安市洪泽区邓码新村A区
浙江省金华市东阳市浙江省金华市东阳市横店镇湖头陆台宏服饰
安徽省宿州市安徽省宣城市宣州区宣城市良苑小区1栋101室，
山东省聊城市高唐县山东省聊城市高唐县山东省聊城市高唐县华泰缔景苑8号楼1单元1301室【货到先致电】
新疆维吾尔自治区直辖县级石河子市新疆维吾尔自治区直辖县级石河子市西公园小区10栋221
浙江省宁波市海曙区浙江省宁波市海曙区轻纺城新龙公寓510室
广东省深圳市龙华新区广东省深圳市龙华区民治上塘路口上塘综合办公楼442
陕西省榆林市榆阳区陕西省榆林市榆阳区建业大道5号高新华府物业办
湖北省直辖县级潜江市湖北省潜江市东风路44号谢想姣
湖南省张家界市永定区湖南省张家界市永定区南庄坪南庄花园
广东省深圳市宝安区广东省深圳市宝安区福永镇重庆路新福工业园佳信德b1栋4楼
云南省大理白族自治州大理市云南省大理白族自治州大理市大理苍海一墅H6一1一5
福建省厦门市湖里区福建省厦门市湖里区金山街道高林社区金林湾花园5号楼19楼1903室，
浙江省温州市鹿城区浙江省温州市鹿城区双屿街道下岭新村永丰超市
江苏省苏州市姑苏区江苏省苏州市姑苏区广济南路新民桥菜市场
四川省成都市天府新区四川省成都市天府新区正兴镇大安路818号
陕西省西安市新城区陕西省西安市新城区韩森寨聚福园小区
云南省曲靖市西苑小区御康花园
广东省东莞市中堂镇广东省东莞市中堂镇沉塘路22号融辉包装材料公司
湖北省黄冈市罗田县湖北省黄冈市罗田县 凤山镇 安居工程3栋102
河北省保定市定州市河北省保定市定州市行定西路阳光公寓小区15号楼
湖北省宜昌市五峰土家族自治县湖北省宜昌市五峰土家族自治县湾潭镇中街15号
贵州省黔南布依族苗族自治州三都水族自治县贵州省黔南布依族苗族自治州三都水族自治县塘州乡
江苏省无锡市江阴市江苏省无锡市江阴市江苏省江阴市石庄新颜路365号501
山东省潍坊市青州市山东省潍坊市青州市潍坊理工学院
陕西省汉中市汉台区陕西省汉中市汉台区宗营镇下宅村三组
江西省赣州市赣县江西省赣州市赣县区沙地镇埠背六福豪庭小区，
湖北省黄冈市黄州区湖北省黄冈市黄州区赤壁街道八一路33号铁友小区6单元  顺丰快递
河北省承德市滦平县河北省承德市滦平县火斗山镇张家沟门村上三岔口134号陈兴转交闫国静
上海上海市青浦区上海市青浦区经徐南路2158弄6号楼
海南省直辖县级乐东黎族自治县海南省直辖县级乐东黎族自治县英歌海镇丰塘村十四队
江苏省宿迁市宿城区江苏省宿迁市宿城区陈集镇工业路1号
浙江省绍兴市诸暨市浙江省绍兴市诸暨市枫桥镇新汽车站103发班室
浙江省绍兴市诸暨市浙江省绍兴市诸暨市枫桥镇新汽车站103发班室
广东省中山市沙溪镇广东省中山市沙溪镇申明亭聚源街八巷一号
浙江省台州市温岭市浙江省台州市温岭市箬横镇东大街霞光北路2号
贵州省六盘水市盘县贵州省六盘水市盘县乐民
广西壮族自治区防城港市东兴市广西壮族自治区防城港市东兴市江平镇巫头村
上海上海市徐汇区上海上海市徐汇区天等路258弄58号602
贵州省毕节市赫章县贵州省毕节市赫章县河镇乡河边村
江苏省宿迁市莱芜区江苏省宿迁市，宿城区，项王小区三期，收件人橙子
江西省赣州市江西省赣州市信丰县信丰工业园工业一路圣塔阳光城金橙花园菜鸟驿站
陕西省咸阳市三原县陕西省咸阳市三原县西阳镇贾里村十三组
贵州省六盘水市盘县贵州省六盘水市盘县淤泥乡
贵州省安顺市平坝区贵州省安顺市平坝区天龙镇
江苏省无锡市滨湖区江苏省无锡市滨湖区凯利公社
河北省邢台市桥东区河北省邢台市桥东区西大街街道塔林公园南侧第一栋一单元401
陕西省渭南市蒲城县陕西省渭南市蒲城县北环路西澳林商贸城
广东省汕头市潮阳区广东省汕头市潮阳区西胪镇内輋，，
青海省海西蒙古族藏族自治州德令哈市青海省蒙古族藏族自治州德令哈市柯鲁柯镇花土村一社
江苏省南通市通州区江苏省南通市通州区平潮镇水岸花都3-1-601
辽宁省大连市庄河市辽宁省大连市瓦房店市体育场凯城公寓四单元306室
云南省红河哈尼族彝族自治州绿春县云南省红河哈尼族彝族自治州绿春县大兴镇牛洪社区落瓦居民小组
广东省佛山市南海区广东省佛山市南海区小塘工业大道联益混凝土有限公司停车场保安室
云南省红河哈尼族彝族自治州云南省红河哈尼族彝族自治州屏边苗族自治县云南省屏边县零开郦水寨16栋
浙江省宁波市浙江省宁波市北仑区榭西工业区兴港路45号金源集团
浙江省宁波市慈溪市浙江省宁波市慈溪市周巷镇海莫社区上品轩8号楼806室
河北省秦皇岛市海港区河北省秦皇岛市海港区金舍鑫苑35-3
陕西省西安市新城区陕西省西安市新城区韩森路长乐公园家属院
吉林省长春市南关区吉林省长春市南关区前进雅苑5-3-708
云南省曲靖市会泽县云南省曲靖市会泽县娜姑镇乐里办事处七组
广西壮族自治区桂林市阳朔县广西壮族自治区桂林市阳朔县金宝乡大桥村大桥街
四川省泸州市合江县四川省泸州市合江县尧坝镇新街兴尧路教师楼一单元
湖南省益阳市桃江县湖南省益阳市桃江县湖南省，益阳市桃江金碧财富F16店
内蒙古自治区乌海市海南区内蒙古自治区乌海市海南区25公里
青海省西宁市湟中县青海省西宁市湟中县西堡镇西两旗法雨寺释因觉
江苏省苏州市张家港市江苏省苏州市张家港市金港镇港区巫山村山前一路26幢150号
山西省运城市夏县山西省运城市夏县瑶峰镇瑶台二区
湖南省益阳市安化县湖南省益阳市安化县渠江镇人民政府
云南省丽江市永胜县云南省丽江市永胜县涛源镇堡子坪（发韵达快递）
山西省长治市城区山西省长治市上党区九鑫家园2 号楼
山东省青岛市西海岸新区山东省青岛市西海岸新区铁橛山路福祉花园
湖南省长沙市开福区湖南省长沙市开福区八一桥旁潇湘华天大酒店工程部检修班
广东省深圳市宝安区广东省深圳市宝安区沙井街道中心路大钟岗二巷47号庆辉大楼
山西省长治市城区山西省长治市上党区九鑫家园2 号楼
江西省吉安市江西省吉安市泰和县澄江镇富瑞一期12栋3单元
天津天津市静海区天津天津市静海区静海镇 天津市静海海吉星A6-105 金锣冷鲜肉
浙江省台州市三门县浙江省台州市三门县浦坝港镇小雄办事处对面婚庆公司
浙江省湖州市吴兴区浙江省湖州市吴兴区爱山街道银泰百货2楼丽芙之心毛绒玩具店
广东省清远市广东省清远市番禺路良江小区21号
云南省德宏傣族景颇族自治州陇川县云南省德宏州陇川县章凤镇三象北路烟草公司对面家良修理厂
云南省文山壮族苗族自治州文山市云南省文山壮族苗族自治州文山市金石路，建设家园对面，缔造尊贵洗鞋店
吉林省长春市南关区吉林省长春市南关区生态东街中海净月华庭5栋1803
贵州省铜仁市碧江区贵州省铜仁市碧江区川硐镇桐达山韵一期
河北省秦皇岛市昌黎县河北省秦皇岛市昌黎县昌黎第一中学
湖北省直辖县级潜江市湖北省潜江市总口管理区蓝天幼儿园
湖北省直辖县级潜江市湖北省潜江市总口管理区蓝天幼儿园
黑龙江省大庆市让胡路区黑龙江省大庆市让胡路区丽水华城水晶园12-2-303
安徽省六安市舒城县安徽省六安市舒城县五显镇梅山村
山西省吕梁市离石区山西省吕梁市离石区李家沟黄金水岸一层货到致电
浙江省杭州市滨江区浙江省杭州市滨江区浙江省杭州市滨江区浦沿街道东冠社区199号
广东省茂名市化州市广东省茂名市化州市石滩农场旁三建项目部
安徽省合肥市安徽省合肥市芜湖路74号安徽省图书馆西一楼大厅办证处
浙江省金华市婺城区浙江省金华市婺州街锦绣国际家居2馆欧饰家卫浴
内蒙古自治区呼和浩特市托克托县内蒙古自治区呼和浩特市托克托县云中花园小区
河北省张家口市蔚县河北省张家口市蔚县滨河湾A2-1-302
广西壮族自治区崇左市扶绥县广西崇左市扶绥县同正大道339号广西城市职业大学
广东省佛山市顺德区广东省佛山市顺德区杏坛雁园新顺路四街15号
青海省玉树藏族自治州玉树市青海省玉树藏族自治州玉树市州新建路（民主路）230号移动分公司
山东省日照市东港区山东省日照市东港区生活印象
湖北省襄阳市襄州区湖北省襄阳市襄州区内燃机车厂
福建省福州市晋安区福建省福州市晋安区新店镇凤池村食杂店中通快递
浙江省嘉兴市秀洲区浙江省嘉兴市秀洲区 旭辉郎香郡24幢401
浙江省绍兴市上虞区浙江省绍兴市上虞区曹娥街道西郊花园38-2-901
贵州省安顺市西秀区贵州省安顺市西秀区若飞北路熙春花园B区C栋1单元203号
湖北省武汉市武昌区湖北省武汉市武昌区秦园东路水岸星城A区G16栋1单元601
浙江省宁波市北仑区浙江省宁波市北仑区浙江省宁波市北仑世茂世界湾1期5幢
陕西省咸阳市三原县陕西省咸阳市三原县大程镇义和村五组
湖南省株洲市石峰区湖南省株洲市石峰区田心火车头美食城
北京北京市永年县北京市西城区右安门内大街宣武师范附属小学26号
广东省清远市英德市广东省清远市英德市大站镇天佑南路花园组1号英红老茶厂！罗成兰收
河北省石家庄市鹿泉区河北省石家庄市鹿泉区李村镇石清公路7号
安徽省合肥市蜀山区安徽省合肥市蜀山区上海城市公寓8栋705
河北省保定市博野县河北省保定市博野县程委镇诚鸿超市
北京北京市丰台区北京北京市丰台区蒲黄榆四巷一排一号
江苏省无锡市江苏省无锡市硕放镇长江东路228号祥生医疗
湖北省黄石市阳新县湖北省黄石市阳新县兴国镇，田家畈，
黑龙江省佳木斯市汤原县黑龙江省佳木斯市汤原县幸福家园快递超市
江苏省镇江市丹阳市江苏省镇江市万善园二村五十七栋一单元401
宁夏回族自治区银川市兴庆区北京东路471号
浙江省衢州市常山县浙江省衢州市常山县翡翠园小区5幢1单元102
贵州省黔西南布依族苗族自治州兴义市 贵州省黔西南布依族苗族自治州兴义市 兴马大道顺发汽车装饰
广东省广州市广东省广州市海珠区赤岗街道康乐蜀湘园
北京北京市昌平区北京市昌平区南口镇南涧路29号北京化工大学(昌平新校区)东门
广西壮族自治区梧州市岑溪市广西壮族自治区梧州市岑溪市归义镇荔枝村
重庆重庆市平山县重庆渝北区园岗路鼎尚名都1栋
福建省福州市台江区福建省福州市台江区祥板路阳光城时代广场9楼
江苏省常州市溧阳市江苏省常州市溧阳市吴谭渡路19号城北工业园《德华机械设备有限公司》
江苏省盐城市建湖县江苏省盐城市建湖县太平新村南区33一6
浙江省台州市天台县浙江省台州市天台县白鹤镇中泽村
河南省洛阳市伊川县河南省洛阳市伊川县太阳绿城
广东省清远市英德市广东省清远市英德市桥头镇政府
河南省新乡市原阳县河南省新乡市原阳县阳阿乡查地村
贵州省黔东南苗族侗族自治州凯里市洗马河街道 凯运大道31号二龙星园亚星宝贝店
湖北省武汉市武昌区湖北省武汉市武昌区常阳丽江城物业处
四川省成都市成华区四川省成都市成华区胜天人居菜鸟驿站
四川省绵阳市四川省绵阳市科创园区玉泉中路19号华润中央公园7栋
贵州省贵阳市花溪区贵州省贵阳市花溪区桐木岭五组
湖北省十堰市竹溪县湖北省十堰市房县仁和路西维娅内衣店
广东省广州市白云区广东省广州市白云区钟落潭镇金盆骑马场一路37号
贵州省贵阳市乌当区贵州省贵阳市乌当区贵州省贵阳市乌当区航天大道保利紫薇郡
山东省日照市东港区山东省日照市东港区山海天旅游度假区文德三区言格教育
湖北省襄阳市襄州区湖北省襄阳市襄州区黄集镇樊魏路202号
辽宁省锦州市义县辽宁省锦州市义县中兴园小区1号楼3单元1楼西户
山东省日照市东港区山东省日照市东港区秦楼街道枣庄路鑫博超市
河南省濮阳市=河南省濮阳市建设路88号
北京北京市东城区北京北京市东城区安定门内大街馅老满252号
福建省漳州市云霄县福建省漳州市云霄县将军大道书香家园宜佳便利店
浙江省温州市永嘉县浙江省温州市永嘉县瓯北镇利民医院
浙江省宁波市江北区浙江省宁波市江北区前江街道裘市村上林房130号
广东省湛江市麻章区广东省湛江市麻章区爱周中学外
广东省茂名市广东省茂名市信宜市金垌镇径口圩
福建省漳州市东山县福建省漳州市东山县福建省漳州市东山县陈城镇宫前村
北京北京市怀柔区北京北京市怀柔区北京市怀柔区桥梓镇口头村
安徽省六安市裕安区安徽省六安市裕安区佛子岭路和顺沁园春雅园19栋902
云南省普洱市宁洱哈尼族彝族自治县云南省普洱市宁洱哈尼族彝族自治县荣升商务酒店
四川省凉山彝族自治州西昌市四川省凉山彝族自治州西昌市宁南县白鹤滩六城营地二区
广东省清远市广东省清远市源潭镇青龙村委会宝鸭塘村。
江苏省常州市江苏省常州市武进区大明路蓝山湖小区
新疆维吾尔自治区巴音郭楞蒙古自治州库尔勒市新疆维吾尔自治区巴音郭楞蒙古自治州库尔勒市建国北路金州一单元301室
浙江省宁波市宁海县浙江省宁波市宁海县黄坦镇木坑湖村
贵州省贵阳市南明区贵州省贵阳市南明区箭道街米市巷40号
内蒙古自治区鄂尔多斯市内蒙古自治区鄂尔多斯市乌审旗一马路鑫世泰小区加佳惠超市
山东省青岛市黄岛区山东省青岛市黄岛区山东省青岛市黄岛区薛家岛街道天目山路亚德里亚海湾1#楼2单元3202室
江西省吉安市江西省吉安市永丰县佐龙乡南塘村
上海上海市奉贤区上海上海市奉贤区青村镇人民路西街2弄1号404
广东省茂名市茂南区广东省茂名市茂南区文光花园18号大院21梯502房
甘肃省 天水市 甘谷县 甘肃省天水市甘谷县甘肃省天水市甘谷一中
四川省遂宁市大英县四川省遂宁市大英县开元新城 开源汽修
江苏省苏州市吴中区江苏省苏州市吴中区胥口镇子胥路333号（胥口粮食储备库）
福建省漳州市福建省漳州市龙海市福建省龙海巿浮宫镇镇前路门号323号
山东省德州市山东省德州市德城区新华办事处大刘新宜家园别墅区32号楼最东户
广西壮族自治区桂林市灵川县广西壮族自治区桂林市灵川县九屋镇加油站对面农机修理店
山东省济宁市梁山县山东省济宁市梁山县山东省济宁市梁山县小路口镇东雷庄社区
四川省泸州市龙马潭区四川省泸州市龙马潭区胡市镇滨河安居
陕西省咸阳市杨陵区陕西省咸阳市杨陵区杨凌示范区医院！
浙江省宁波市余姚市浙江省宁波市余姚市泗门镇湖北中心西路62号
湖北省十堰市张湾区湖北省十堰市张湾区车城西路81号里金星花园
甘肃省兰州市西固区甘肃省兰州市西固区兰西铁院门口
广东省佛山市广东省佛山市高明区荷城文华路玉泉街20号
陕西省西安市莲湖区陕西省西安市莲湖区龙首荣民购物广场一楼华为体验店
陕西省延安市陕西省延安市安塞县紫金兰小区
四川省南充市仪陇县四川省南充市仪陇县金城镇瑞泉路33号
江苏省泰州市海陵区江苏省泰州市海陵区迎春西路动力小区5号楼301室
浙江省舟山市定海区浙江省舟山市定海区干览镇 兴业路1号浙江兴业集团有限公司11号楼（货到电联）
安徽省阜阳市颍东区安徽省阜阳市颍东区向阳街道肉连厂对面国通快递3号仓库D11
江西省抚州市南城县江西省抚州市南城县建昌镇聚福星城
河南省三门峡市河南省三门峡市灵宝市尹庄镇李村塑料厂
浙江省台州市椒江区浙江省台州市椒江区葭沚街道 红星村35幢（东边第一间）
云南省红河哈尼族彝族自治州蒙自市云南省红河哈尼族彝族自治州云南省红河州蒙自市圣路苑9一4（住房），林晓艳，
江苏省镇江市丹阳市江苏省镇江市丹阳市丹北镇新桥金江工业园西区20号
广东省佛山市广东省佛山市南海区桂城街道依岸康提花园44座2603
湖北省黄冈市蕲春县湖北省黄冈市蕲春县蕲州镇赤东湖鱼场
安徽省芜湖市无为县安徽省芜湖市无为县龙庵镇景湖小区
广东省中山市小榄镇广东省中山市小榄镇宝丰社区新龙北街47号
陕西省延安市陕西省延安市宝塔区陕西省延安市宝塔区南二十里铺桃园小区
河北省廊坊市霸州市河北省廊坊市霸州金康雅居四号楼一单元2403
湖南省长沙市天心区湖南省长沙市天心区桂花坪街道金桂小区A53栋3门
宁夏回族自治区银川市金凤区宁夏回族自治区银川市金凤区阅海万家F2区29号楼
河南省周口市沈丘县河南省周口市沈丘县付井镇付井中心街
天津天津市宝坻区天津天津市宝坻区金玉四园12号楼303
浙江省金华市收货人:陈海青  所在地区:浙江省 金华市 磐安县 详细地址:方前镇磐天路26号叶万旺
浙江省温州市泰顺县浙江省温州市泰顺县地址：浙江温州泰顺士阳镇
浙江省温州市浙江省温州市瑞安市马屿镇凤鹤路217号
河北省廊坊市三河市河北省廊坊市三河市泃阳镇高各庄村
贵州省铜仁市碧江区贵州省铜仁市碧江区共青路盛世铭城5栋
河北省廊坊市三河市河北省廊坊市三河市泃阳镇高各庄村
河北省邢台市隆尧县河北省邢台市隆尧县莲子镇镇东方食品城东范村（客户要求19号签收）
广东省深圳市宝安区广东省深圳市宝安区沙井街道办共和社区湾厦工业园６栋５楼
山东省青岛市黄岛区山东省青岛市黄岛区红石崖尚景嘉园菜鸟驿站（安排顺丰）
广东省深圳市宝安区广东省深圳市宝安区沙井街道办共和社区湾厦工业园６栋５楼
广东省广州市白云区广东省广州市白云区均禾街道桃红西街金源广场3栋一楼(私人件)
重庆重庆市重庆市沙坪坝区青木关镇明阳酒店旁
山东省济南市市中区山东省济南市市中区十六里河街道融汇爱都北门宠爱宠物生活会馆
内蒙古自治区鄂尔多斯市内蒙古鄂尔多斯东胜区纺织街道金园小区1号楼
山东省青岛市胶州市山东省青岛胶州市北关御景苑【 货到先致电】
浙江省台州市天台县浙江省台州市天台县白鹤镇中泽村
广东省东莞市东城区广东省东莞市东城区周屋围四巷十八号
浙江省金华市义乌市浙江省金华市义乌市 北苑街道望道路125号黄杨梅商住楼1单元1105
江西省赣州市安远县江西省赣州市安远县东江源小学对面深夜豆浆
北京北京市海淀区北京北京市海淀区西直门北大街 56号 富德生命人寿1006室
重庆重庆市黔江区重庆市黔江区濯水古镇山门村五组
安徽省滁州市南谯区安徽省滁州市南谯区龙蟠街道，名儒园，3栋2004
陕西省西安市雁塔区陕西省西安市雁塔区西影路46号西勘小区
江苏省常州市武进区江苏省常州市武进区经济开发区礼河镇 蠡河新苑7－5
广东省茂名市高州市广东省茂名市高州市第一中学，飞飞收
宁夏回族自治区银川市金凤区宁夏回族自治区银川市金凤区泰康街银新苑A3区
辽宁省大连市辽宁省大连市甘井子区虹港新居211－1－1－2.
贵州省毕节市大方县贵州省毕节市大方县核桃乡烂泥沟街上
上海上海市崇明县上海上海市崇明县港沿镇合兴北沿公路669号
江苏省苏州市昆山市江苏省苏州市昆山市玉山镇北门路都市汇五楼
广东省东莞市塘厦镇广东省东莞市塘厦镇振兴围草朗6号
辽宁省大连市瓦房店市辽宁省大连市长兴岛临港工业区三堂兰亭假日小区
浙江省台州市温岭市浙江省台州市温岭市城北街道后湾富明星鞋厂
江西省赣州市安远县江西省赣州市安远县东江源小学对面深夜豆浆【货到先致电。】
广东省东莞市高埗镇广东省东莞市高埗镇莞香水果市场5号门
四川省成都市成华区四川省成都市成华区槐树店鲁能城二期二栋，
广东省东莞市常平镇广东省东莞市常平镇大京九农副产品批发市场（1008）档.收，
北京北京市北京北京市昌平区北七家镇企业服务中心
云南省昆明市官渡区云南省昆明市官渡区牛街庄贵昆路506号
内蒙古自治区鄂尔多斯市内蒙古自治区鄂尔多斯市东胜区华莹南区3号楼收电话
内蒙古自治区巴彦淖尔市临河区内蒙古巴彦淖尔市临河区新华镇永乐四社
广东省汕尾市城区广东省...汕尾市.....城区.....吉祥路...吉港大厦
广东省东莞市东坑镇广东省东莞市东坑镇黄麻岭村正威五路28号金利源便利店
天津天津市天津天津市和平区大沽北路65号汇金中心2301室
四川省广安市华蓥市四川省广安市华蓥市新华大道华蓥市人民法院星月收
四川省内江市东兴区四川省内江市东兴区汉安大道中段门面200号银华石材【货到先致电】
宁夏回族自治区固原市泾源县宁夏回族自治区固原市泾源县香水镇丽景园B区，,
北京北京市顺义区北京市顺义区仁和花园一区35号楼二单元1502
河南省信阳市平桥区河南省信阳市平桥区平西路馨澳花园门卫室
江苏省南京市六合区江苏省南京市六合区莉湖花园3幢202室
福建省福州市鼓楼区福建省福州市鼓楼区江厝路省直湖前小区38座702
天津天津市天津市蓟州区盘山大道68号大学城
湖南省长沙市望城区湖南省长沙市望城区高塘岭街道湖南电子科技新校区
江苏省盐城市射阳县江苏省盐城市射阳县水木清华苑 2#楼
广东省茂名市茂南区广东省茂名市茂南区碧桂园城央首府金益庭生活区
广东省佛山市南海区广东省佛山市南海区九江沙口综合市场水果街3号
云南省曲靖市沾益区云南省曲靖市沾益区云南省曲靖市沾益区白水镇凤祥南路6号
福建省宁德市霞浦县福建省宁德市霞浦县溪南镇台江村
山西省运城市盐湖区山西省运城市盐湖区 八一市场西口中环大厦
广东省深圳市光明新区广东省深圳市光明新区玉律村维珍妮C厂
广东省深圳市龙岗区广东省深圳市龙岗区龙新社区沙背沥二路92栋一楼瑞泰玻璃厂 高级健康顾问
福建省南平市福建省南平市顺昌县双溪街道新屯村98号
福建省龙岩市上杭县福建省龙岩市上杭县临江镇解放路410号晓英美发
上海上海市浦东新区上海上海市浦东新区王巷镇新虹村陆家宅94号
浙江省宁波市杭州湾新区浙江省宁波市杭州湾新区世纪城梦想公寓8栋
湖北省孝感市汉川市湖北省孝感市汉川市银湖城11－2单元
河北省秦皇岛市卢龙县河北省秦皇岛市卢龙县肥子路大金地产 苏 彤
广东省东莞市横沥镇广东省东莞市横沥镇石冲村北四巷1号
山东省临沂市莒南县山东省临沂市莒南县山南县莒坪上镇厉家寨村中兴商务中心
云南省曲靖市宣威市云南省宣威市向阳西街和丰雅苑
广东省中山市大涌镇广东省中山市大涌镇和涌路168号之一（大自然格瑞）
广东省深圳市龙华新区广东省深圳市龙华新区深圳市龙华区福城街道招商锦绣观园1栋
湖南省郴州市安仁县湖南省郴州市安仁县七一西路顶上整装定制店
福建省福州市福建省福州市闽侯县福建省福州市闽侯县上街镇新保路18号正荣财富中心11#二层物业处
浙江省宁波市北仑区浙江省宁波市北仑区龙潭山路七号
江苏省苏州市昆山市江苏省苏州市昆山市玉山镇娄汀苑10号楼1701室
江西省抚州市江西省抚州市东乡区孝岗镇龙山小学（客要求顺丰）
四川省广元市利州区四川省广元市利州区云盘梁瞻凤路福安家园1栋1单元1楼2号
内蒙古自治区鄂尔多斯市达拉特旗内蒙古自治区鄂尔多斯市达拉特旗树林召镇东源国际广场10#楼27号底店
贵州省贵阳市观山湖区贵州省贵阳市观山湖区世纪城龙凯苑23栋
湖南省长沙市望城区湖南省长沙市望城区雷锋镇雷锋机电市场重建地
陕西省西安市高陵区陕西省西安市高陵区泾渭街道马家湾水榭中央领地
新疆维吾尔自治区吐鲁番市鄯善县新疆维吾尔自治区吐鲁番市鄯善县火车站镇吐哈油田公寓
上海上海市宝山区上海市宝山区宝钢九村63号301室
河南省郑州市中原区河南省郑州市中原区陇海西路65号院郑州外国语中学
四川省攀枝花市米易县四川省攀枝花市米易县克朗
江苏省南通市江苏省南通市崇川区江海镇区 详细地址: 江达路通富微电有限公司江达路99号
云南省德宏傣族景颇族自治州芒市云南省德宏州芒市镇广母村广母小学斜对面，正新轮胎店
广西壮族自治区南宁市西乡塘区石埠街道鹏飞路15号广西工商职业技术学院
四川省巴中市巴州区四川省巴中市巴州区四白云台凯悦名城B区16栋1001
河北省秦皇岛市卢龙县河北省秦皇岛市卢龙县下寨乡石家脑村
浙江省绍兴市诸暨市浙江省绍兴市诸暨市陶朱街道龙山社区庙坞240号
山东省潍坊市寒亭区山东省潍坊市寒亭区山东省潍坊市寒亭区央子街道昊海大街滨海石化职工宿舍
广东省广州市海珠区广东省广州市海珠区沙园街道西基西21一1 【顺丰】
广西壮族自治区南宁市江南区广西壮族自治区南宁市江南区壮锦大道36号碧园南城故事53栋
四川省巴中市巴州区四川省巴中市巴州区巴中市巴州区西城国际商汇 海洋乐园大门口
上海上海市浦东新区上海上海市浦东新区泥城镇妙香路1318号
广东省阳江市广东省阳江市阳东区平南路幸福园（顺丰）
河南省信阳市平桥区河南省信阳市平桥区前进街道 新六大街长虹百花沁园16号楼
广东省广州市越秀区广东省广州市越秀区东华北路52号广东商业大夏丰巢快递柜
广东省深圳市龙华新区广东省深圳市深圳市龙华区民治街道白石龙路23号
天津天津市北辰区天津天津市北辰区北辰西道双青盛泰园7号楼
河南省周口市河南省周口市开元大道周口职业技术学院
广西壮族自治区百色市西林县广西壮族自治区百色市西林县西林县
新疆维吾尔自治区昌吉回族自治州昌吉市新疆维吾尔自治区昌吉回族自治州昌吉市北京北路132号昌吉州政协【货到提前电联】上班时间派送，不要休息日派送，休息日客户不上班
湖北省武汉市汉阳区湖北省武汉市汉阳区七里庙汉钢路4号雅丽花园一期4—4—201 c
四川省达州市四川省达州市通川区达州市通川区翠屏路院棚巷洋品店
新疆维吾尔自治区塔城地区托里县新疆维吾尔自治区塔城地区托里县川渝花园2号楼5单元502.
江苏省苏州市吴中区江苏省苏州市吴中区兴南路19号，二号楼福慧达光电有限公司，小薇，136****4510
北京北京市朝阳区北京北京市朝阳区南湖西园235楼202
河南省郑州市中牟县河南省郑州市中牟县白沙镇通惠路与青年路交叉口红枫树酒店七楼
陕西省汉中市洋县陕西省汉中市洋县唐塔北路粮食局院内
黑龙江省大庆市萨尔图区黑龙江省大庆市萨尔图区万宝二区盛大德水暖五金店货到先打电话约好时间配送
山西省晋城市山西省晋城市凤苑小区19号楼302
江苏省苏州市常熟市江苏省苏州市常熟市南沙路香溢璟庭17幢102室
四川省广元市利州区四川省广元市利州区南河滨河南路27号，爱车之家
辽宁省沈阳市皇姑区辽宁省沈阳市皇姑区塔湾街道塔湾新北行农贸市场西四门38号档口
湖南省益阳市赫山区湖南省益阳市赫山区龙光桥镇叶家河村神龙米业。
浙江省金华市浦江县浙江省金华市浦江县浙江省金华市浦江县岩头镇芳地村
浙江省宁波市北仑区浙江省宁波市北仑区宝山路东方港城三期 23栋 2601
北京北京市海淀区北京北京市海淀区复兴路23号院西南门
内蒙古自治区鄂尔多斯市东胜区内蒙古自治区鄂尔多斯市东胜区纺织街郝家圪卜路华研生活小区20号楼一单元404
内蒙古自治区鄂尔多斯市东胜区内蒙古自治区鄂尔多斯市东胜区纺织街郝家圪卜路华研生活小区20号楼一单元404
江苏省常州市钟楼区江苏省常州市钟楼区北港街道星港路65号格力博集团蒙宣美食
内蒙古自治区呼和浩特市回民区内蒙古自治区呼和浩特市回民区 回民区 新华桥西街四合兴小区一号车棚
四川省巴中市通江县四川省巴中市通江县新场镇下街286号
黑龙江省哈尔滨市平房区黑龙江省哈尔滨市平房区友协东头道街238楼肤医堂
福建省南平市建阳区福建省南平市建阳区童游街道朱熹大道山水名郡3443号am美发店
贵州省黔东南苗族侗族自治州黎平县贵州省黔东南黎平县五开北路大地家园C栋601
内蒙古自治区鄂尔多斯市伊金霍洛旗内蒙古自治区鄂尔多斯市伊金霍洛旗札萨克镇新街
河南省三门峡市河南省三门峡市建设路一高对面秦镇米皮
江苏省南京市浦口区江苏省南京市浦口区江苏省南京市浦口区大桥北路37号金都汇广场2栋1101
浙江省温州市鹿城区浙江省温州市鹿城区七都街道前沙村前进路76号
广东省广州市黄埔区广东省广州市黄埔区丰乐北路豫章苑
广东省清远市清新区广东省清远市清新区三坑镇陂头村委会陂头加油站
河南省漯河市临颍县河南省漯河市临颍县财政局对面
福建省厦门市湖里区福建省厦门市湖里区厦门市湖里区金尚小区金安里56栋55号803
安徽省阜阳市阜南县安徽省阜阳市阜南县十八里铺镇
安徽省滁州市琅琊区安徽省滁州市琅琊区城东花园 7栋
河北省邯郸市永年县河北省邯郸市永年县永合会镇 大油村
浙江省台州市临海市浙江省台州市临海市东湖路13号   顺丰
陕西省西安市曲江新区陕西省西安市曲江文化运动公园南门
上海上海市徐汇区上海市徐汇区太原路181弄3号后门（顺丰 ）
广西壮族自治区河池市东兰县广西壮族自治区河池市东兰县新城路206号
广东省惠州市惠城区广东省惠州市惠城区河南岸石湖苑 7栋E座403
浙江省杭州市余杭区浙江省杭州市余杭区仓前街道科技大道中交二航局搅拌站
山东省泰安市新泰市山东省泰安市新泰市汶南镇陈粮村
江西省宜春市上高县江西省宜春市上高县东丰路8号(御山华府小区)晏祖荣收
湖北省直辖县级潜江市湖北省直辖县级潜江市后湖管理区后湖大道106号中国电信营业厅
广东省深圳市宝安区广东省深圳市龙岗区龙城街道长江花园4单元513
福建省福州市福建省福州市闽侯县祥谦镇山前工业区星联汽车配件有限公司
广西壮族自治区贵港市平南县广西壮族自治区贵港市平南县武林镇
河南省三门峡市湖滨区河南省三门峡市湖滨区大岭南路兴业公寓1号楼一单元2楼西户
青海省西宁市城中区青海省西宁市城中区城中区砖厂路7号
安徽省马鞍山市含山县安徽省马鞍山市含山县经济开发区凌家滩路491号安徽朗迪叶轮机械有限公司
重庆重庆市合川区重庆重庆市合川区钓鱼城街道600号江润北城天地33栋
湖北省宜昌市宜都市湖北省宜昌市宜都市宜都市陆城夷水路撞球帮院内木材销售
四川省成都市四川省成都市崇州市金带街227号
甘肃省庆阳市西峰区甘肃省庆阳市西峰区太一地中海北门
辽宁省阜新市阜新蒙古族自治县辽宁省阜新市阜新蒙古族自治县 详细地址:辽宁省阜新市阜蒙县福兴地镇
上海上海市松江区上海上海市松江区谷阳北路1066弄270号602室
浙江省台州市路桥区浙江省台州市路桥区金清镇黄琅工业区茹迪家具厂
江苏省盐城市亭湖区江苏省盐城市亭湖区南洋镇曙光村四组
浙江省金华市金东区浙江省金华市金东区环城南路571
上海上海市宝山区上海上海市宝山区场中路3707号
湖南省益阳市赫山区湖南省益阳市赫山区龙光桥镇叶家河村神龙米业。
黑龙江省哈尔滨市南岗区黑龙江省哈尔滨市南岗区王岗旗凯丽园16栋4单元1101
广东省惠州市惠阳区广东省惠州市惠阳区淡水南湖北路13号江湖渔道店
浙江省杭州市桐庐县大奇山路罗兰公寓25幢401室
海南省三亚市吉阳区海南省三亚市吉阳区荔枝沟路140-1号雷必成诊所
重庆重庆市重庆市南岸区海棠新街1号聚丰江山里客服中心
山西省晋城市阳城县山西省晋城市阳城县町店镇柴凹村
山东省济宁市山东省济宁市汶上县南站镇唐阳煤矿
山东省济宁市汶上县山东省济宁市汶上县中都大街北段美鲜冻品行";

        $arr = explode("\n", $str);
        $html = "";
        foreach ($arr as $line) {
            $res = AddressParse::getDetail($line);
            $html .= "{$line}       ==================>         " . $res['address'] . "<br/>";
        }
        return $html;
    }
}
