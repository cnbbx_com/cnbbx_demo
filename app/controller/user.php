<?php

namespace app\controller;

use app\middleware\InUserCheck;
use app\model\wishing;
use cnbbx\Controller;
use cnbbx\View;

class user extends Controller
{

    /**
     * 中间件测试
     * @var array
     */
    protected $middleware = [InUserCheck::class];

    /**
     * 注册或者登录
     * @return string
     */
    public function sign()
    {
        return View::fetch('sign');
    }

    /**
     * 表单页面
     * @return string
     */
    public function submit()
    {
        $user = input('user');
        $wishing = input('wishing');
        if (!empty($wishing)) {
            $model = new wishing();
            $model->insert(['user' => $user, 'wishing' => $wishing]);
            return "<script language='javascript'>alert('许愿成功');top.location='/';</script>";
        }
        return View::fetch('submit');
    }

    /**
     * 数据库测试
     * @return mixed[]
     */
    public function index()
    {
        $wishing = new wishing();
        return $wishing->eq('id', 1)->select();
    }

    /**
     * 请求参数测试
     * @param string $id
     * @param string $name
     * @return string
     */
    public function request($id = '', $name = '')
    {
        dump($id);
        dump($name);
        dump(input('test'));
        return '<h1>login</h1>';
    }
}
