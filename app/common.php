<?php

use app\middleware\InAppCheck;
use cnbbx\Route;

// 以下暂时未完成
Route::rule('new/:id', 'News/read', 'GET|POST')->middleware([InAppCheck::class]);
Route::get('new/:cate$', 'News/category');
Route::get('new/:name', 'News/read')->pattern(['name' => '[\w|\-]+']);
Route::get('cnbbx', function () {
    return 'hello,cnbbx!';
});
Route::get('item-<name>-<id>', 'product/detail')->pattern(['name' => '\w+', 'id' => '\d+']);
Route::get('hello/:name', 'index/hello');
Route::rule(':app/bigFile/:function', 'bigFile/:function', 'GET|POST');