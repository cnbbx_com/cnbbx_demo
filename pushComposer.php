<?php
$url = 'https://packagist.org/api/update-package?username=cnbbx&apiToken=v4_L0cM1xtJP3okyp_P5';
$headers = [
    'Content-Type:application/json',
];
$data = '{"repository":{"url":"https://packagist.org/packages/cnbbx_com/cnbbx"}}';
if (isset($_SERVER['HTTP_X_GITEE_TOKEN']) && $_SERVER['HTTP_X_GITEE_TOKEN'] == 'cnbbx123') {
    $res = curl_post($url, $data, $headers);
} else {
    $res = 'password error!';
}
echo $res;
function curl_post($url, $data = '', $headers = []) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    if ($headers) {
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, 0);
    }
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);// POST数据
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}