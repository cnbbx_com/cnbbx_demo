<?php

namespace common\status;

class GoodMorning implements IState {
    /**
     * @param Work $w
     * @return string
     */
    public function WriteCode($w) {
        if ($w->hour < 9) {
            return '早上好';
        } else {
            $w->SetState(new GoodForenoon());
            return $w->WriteCode();
        }
    }
}