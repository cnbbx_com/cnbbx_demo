<?php

namespace common\status;

class GoodNoon implements IState {
    /**
     * @param Work $w
     * @return mixed
     */
    public function WriteCode($w) {
        if ($w->hour < 14) {
            return '中午好';
        } else {
            $w->SetState(new GoodAfternoon());
            return $w->WriteCode();
        }
    }
}