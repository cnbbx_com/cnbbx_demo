<?php

namespace common\status;

class GoodAfternoon implements IState {
    /**
     * @param Work $w
     * @return string
     */
    public function WriteCode($w) {
        if ($w->hour < 17) {
            return '下午好';
        } else {
            $w->SetState(new GoodDusk());
            return $w->WriteCode();
        }
    }
}