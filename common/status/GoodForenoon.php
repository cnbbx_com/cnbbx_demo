<?php

namespace common\status;

class GoodForenoon implements IState {
    /**
     * @param Work $w
     * @return mixed
     */
    public function WriteCode($w) {
        if ($w->hour < 12) {
            return '上午好';
        } else {
            $w->SetState(new GoodNoon());
            return $w->WriteCode();
        }
    }
}