<?php

namespace common\status;

class GoodNight implements IState {
    /**
     * @param Work $w
     * @return mixed
     */
    public function WriteCode($w) {
        if ($w->hour < 22) {
            return '晚上好';
        } else {
            $w->SetState(new GoodAtNight());
            return $w->WriteCode();
        }
    }
}