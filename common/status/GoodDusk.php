<?php

namespace common\status;

class GoodDusk implements IState {
    /**
     * @param Work $w
     * @return mixed
     */
    public function WriteCode($w) {
        if ($w->hour < 19) {
            return '傍晚好';
        } else {
            $w->SetState(new GoodNight());
            return $w->WriteCode();
        }
    }
}