<?php

namespace common\status;

class Work {
    public $hour;
    private $current;

    public function __construct() {
        $this->current = new EarlyMorning();
    }

    //设置状态
    public function SetState($s) {
        $this->current = $s;
    }

    public function WriteCode() {
        return $this->current->WriteCode($this);
    }
}