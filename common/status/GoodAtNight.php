<?php

namespace common\status;

class GoodAtNight implements IState {
    /**
     * @param Work $w
     * @return mixed
     */
    public function WriteCode($w) {
        if ($w->hour < 23) {
            return '夜间好';
        } else {
            $w->SetState(new Midnight());
            return $w->WriteCode();
        }
    }
}