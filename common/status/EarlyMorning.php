<?php

namespace common\status;

class EarlyMorning implements IState {
    /**
     * @param Work $w
     * @return string
     */
    public function WriteCode($w) {
        if ($w->hour < 6) {
            return '早晨好';
        } else {
            $w->SetState(new GoodMorning());
            return $w->WriteCode();
        }
    }
}