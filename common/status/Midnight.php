<?php

namespace common\status;

class Midnight implements IState {
    /**
     * @param Work $w
     * @return string
     */
    public function WriteCode($w) {
        return '半夜好';
    }
}