<?php

namespace cnbbx;

defined('DS') or define('DS', DIRECTORY_SEPARATOR);
define('BASE_PATH', __DIR__ . DS . ".." . DS);

require __DIR__ . '/../vendor/autoload.php';

$http = new Http;
$http->run('swoole', 10086);